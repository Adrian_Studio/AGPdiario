import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AccountingGraphsComponent } from './pages/accounting/accounting-graphs/accounting-graphs.component';
import { AccountingActivityTableComponent } from './pages/accounting/accounting-activity-table/accounting-activity-table.component';
import { AccountingHeaderComponent } from './pages/accounting/accounting-header/accounting-header.component';
import { VehicleHeaderComponent } from './pages/vehicle/vehicle-header/vehicle-header.component';
import { DropdownsHeaderComponent } from './pages/dropdowns/dropdowns-header/dropdowns-header.component';
import { AccountingMonthlySummaryComponent}  from './pages/accounting/accounting-monthly-summary/accounting-monthly-summary.component';
import { AccountingAnnualSummaryComponent } from './pages/accounting/accounting-annual-summary/accounting-annual-summary.component';
import { InventoryHeaderComponent } from './pages/inventory/inventory-header/inventory-header.component';
import { TripsHeaderComponent } from './pages/trips/trips-header/trips-header.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { AuthGuard } from './security/auth.guard';


const routes: Routes = [
  {
    path: '',   
    redirectTo: 'accountingGraphs',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  // Para la personal no quiero la posibilidad de que se registre nadie nuevo (Se registra manualmente desde Firebase)
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'accountingGraphs',
    component: AccountingHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'accountingGraphs' }
  },
  {
    path: 'accountingTable',
    component: AccountingHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'accountingTable' }
  },
  {
    path: 'accountingMonthlySummary',
    component: AccountingHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'accountingMonthlySummary' }
  },
  {
    path: 'accountingAnnualSummary',
    component: AccountingHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'accountingAnnualSummary' }
  },
  {
    path: 'vehicleGraphs',
    component: VehicleHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'vehicleGraphs' }
  },
  {
    path: 'vehicleGas',
    component: VehicleHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'vehicleGas' }
  },
  {
    path: 'vehicleMaintenance',
    component: VehicleHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'vehicleMaintenance' }
  },
  {
    path: 'vehicleFinancing',
    component: VehicleHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'vehicleFinancing' }
  },
  {
    path: 'inventory',
    component: InventoryHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'inventory' }
  },
  {
    path: 'trips',
    component: TripsHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'trips' }
  },
  {
    path: 'accountingCategories',
    component: DropdownsHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'accountingCategories' }
  },
  {
    path: 'accountingConcepts',
    component: DropdownsHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'accountingConcepts' }
  },
  {
    path: 'houses',
    component: DropdownsHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'houses' }
  },
  {
    path: 'vehicles',
    component: DropdownsHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'vehicles' }
  },
  {
    path: 'inventoryType',
    component: DropdownsHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'inventoryType' }
  },
  {
    path: 'inventoryLocation',
    component: DropdownsHeaderComponent,
    canActivate: [AuthGuard],
    data: { selectedPage: 'inventoryLocation' }
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: 'not-found'
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
