import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { environment } from '../environments/environment';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';

import { AccountingGraphsComponent } from './pages/accounting/accounting-graphs/accounting-graphs.component';
import { AccountingHeaderComponent } from './pages/accounting/accounting-header/accounting-header.component';
import { AccountingMonthlySummaryComponent}  from './pages/accounting/accounting-monthly-summary/accounting-monthly-summary.component';
import { AccountingAnnualSummaryComponent } from './pages/accounting/accounting-annual-summary/accounting-annual-summary.component';
import { VehicleGraphsComponent } from './pages/vehicle/vehicle-graphs/vehicle-graphs.component';
import { VehicleGasComponent } from './pages/vehicle/vehicle-gas/vehicle-gas.component';
import { VehicleMaintenanceComponent } from './pages/vehicle/vehicle-maintenance/vehicle-maintenance.component';
import { VehicleFinancingComponent } from './pages/vehicle/vehicle-financing/vehicle-financing.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { LoginComponent } from './pages/login/login.component';

import { DataTablesModule } from 'angular-datatables';
import { InventoryItemsConsultsService } from './services/inventory/inventory-items-consults.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UserService } from './services/users/user.service';
import { RegisterComponent } from './pages/register/register.component';
import { NavBarComponent } from './pages/nav-bar/nav-bar.component'

import { AuthGuard } from './security/auth.guard';
import { InventoryItemTableComponent } from './pages/inventory/inventory-item-table/inventory-item-table.component';
import { InventoryItemFormComponent } from './pages/inventory/inventory-item-form/inventory-item-form.component';
import { InventoryElementTypeFormComponent } from './pages/dropdowns/inventory-element-type-form/inventory-element-type-form.component';
import { InventoryElementTypeTableComponent } from './pages/dropdowns/inventory-element-type-table/inventory-element-type-table.component';
import { InventoryLocationTableComponent } from './pages/dropdowns/inventory-location-table/inventory-location-table.component';
import { InventoryLocationFormComponent } from './pages/dropdowns/inventory-location-form/inventory-location-form.component';
import { ChartsModule } from 'ng2-charts';

import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { AccountingActivityFormComponent } from './pages/accounting/accounting-activity-form/accounting-activity-form.component';
import { AccountingCategoryFormComponent } from './pages/dropdowns/accounting-category-form/accounting-category-form.component';
import { AccountingConceptFormComponent } from './pages/dropdowns/accounting-concept-form/accounting-concept-form.component';
import { HouseFormComponent } from './pages/dropdowns/house-form/house-form.component';
import { VehicleFormComponent } from './pages/dropdowns/vehicle-form/vehicle-form.component';
import { AccountingCategoryTableComponent } from './pages/dropdowns/accounting-category-table/accounting-category-table.component';
import { AccountingConceptTableComponent } from './pages/dropdowns/accounting-concept-table/accounting-concept-table.component';
import { HouseTableComponent } from './pages/dropdowns/house-table/house-table.component';
import { VehicleTableComponent } from './pages/dropdowns/vehicle-table/vehicle-table.component';
import { AccountingActivityTableComponent } from './pages/accounting/accounting-activity-table/accounting-activity-table.component';
import { AccountingFilterFormComponent } from './pages/accounting/accounting-filter-form/accounting-filter-form.component';
import { DropdownsHeaderComponent } from './pages/dropdowns/dropdowns-header/dropdowns-header.component';
import { VehicleHeaderComponent } from './pages/vehicle/vehicle-header/vehicle-header.component';
import { TripsHeaderComponent } from './pages/trips/trips-header/trips-header.component';
import { TripsMapComponent } from './pages/trips/trips-map/trips-map.component';
import { TripsTableComponent } from './pages/trips/trips-table/trips-table.component';
import { InventoryHeaderComponent } from './pages/inventory/inventory-header/inventory-header.component';
import { TripsFormComponent } from './pages/trips/trips-form/trips-form.component';
import { VehicleFilterFormComponent } from './pages/vehicle/vehicle-filter-form/vehicle-filter-form.component';


// the second parameter 'es' is optional
registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [
    AppComponent,
    AccountingGraphsComponent,
    AccountingHeaderComponent,
    NotFoundComponent,
    AccountingMonthlySummaryComponent,
    AccountingAnnualSummaryComponent,
    VehicleGraphsComponent,
    VehicleGasComponent,
    VehicleMaintenanceComponent,
    VehicleFinancingComponent,
    LoginComponent,
    RegisterComponent,
    NavBarComponent,
    InventoryItemTableComponent,
    InventoryItemFormComponent,
    InventoryElementTypeFormComponent,
    InventoryElementTypeTableComponent,
    InventoryLocationTableComponent,
    InventoryLocationFormComponent,
    AccountingActivityFormComponent,
    AccountingCategoryFormComponent,
    AccountingConceptFormComponent,
    HouseFormComponent,
    VehicleFormComponent,
    AccountingCategoryTableComponent,
    AccountingConceptTableComponent,
    HouseTableComponent,
    VehicleTableComponent,
    AccountingActivityTableComponent,
    AccountingFilterFormComponent,
    DropdownsHeaderComponent,
    VehicleHeaderComponent,
    TripsHeaderComponent,
    TripsMapComponent,
    TripsTableComponent,
    InventoryHeaderComponent,
    TripsFormComponent,
    VehicleFilterFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    AngularFontAwesomeModule,
    NgbModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      countDuplicates: true,
      newestOnTop: true,
      progressBar:true,
      resetTimeoutOnDuplicate: true,
    }),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    DataTablesModule,
    ChartsModule,
  ],
  providers: [
    InventoryItemsConsultsService,
    UserService,
    AngularFireAuth,
    AuthGuard,
    HttpClientModule,
    NavBarComponent,
    AccountingHeaderComponent
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    InventoryItemFormComponent,
    InventoryElementTypeFormComponent,
    InventoryLocationFormComponent,
    NavBarComponent,
    AccountingActivityFormComponent,
    AccountingCategoryFormComponent,
    AccountingConceptFormComponent,
    HouseFormComponent,
    VehicleFormComponent,
    AccountingFilterFormComponent,
    TripsFormComponent,
    VehicleFilterFormComponent,
],
})
export class AppModule { }
