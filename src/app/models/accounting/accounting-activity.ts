export class AccountingActivity {
    $key: string;
    type: string;
    categoryKey: string;
    category: string;
    conceptKey: string;
    concept: string;
    date: string;
    value: number;
    remark: string;
    house: string;
    vehicle: string;
    vehicleName: string;
    milometer: number;
    litres: number;
}
