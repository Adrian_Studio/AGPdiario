export class AccountingFilter {
    type: string[];
    category: string[];
    concept: string[];
    startDate: string;
    endDate: string;
    year: any;
    month: string[];
    house: string[];
    vehicle: string[];
}
