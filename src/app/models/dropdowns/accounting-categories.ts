export class AccountingCategories {
    $key: string;
    type: string;
    category: string;
}
