export class AccountingConcepts {
    $key: string;
    type: string;
    category: string;
    concept: string;
}
