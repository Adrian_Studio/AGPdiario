export class Vehicles {
    $key: string;
    brand: string;
    model: string;
    plate: string;
}
