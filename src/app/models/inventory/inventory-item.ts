export class InventoryItem {
    $key: string;
    location: string;
    itemType: string;
    item: string;
    acquisitionPlace: string;
    model: string;
    identifier: string;
    acquisitionValue: number;
    acquisitionDate: string;
    lossValue: number;
    lossDate: string;
    state: string;
    remark: string;
}
