export class Trip {
    $key: string;
    date: string;
    continentName: string;
    continent: string;
    countryName: string;
    country: string;
    place: string;
    latitude: number;
    longitude: number;
    remark: string;
}
