import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingActivityFormComponent } from './accounting-activity-form.component';

describe('AccountingActivityFormComponent', () => {
  let component: AccountingActivityFormComponent;
  let fixture: ComponentFixture<AccountingActivityFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingActivityFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingActivityFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
