import { Component, OnInit, Inject, ChangeDetectorRef} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { AccountingActivityConsultsService } from '../../../services/accounting/accounting-activity-consults.service';
import { AccountingCategoryConsultsService } from '../../../services/dropdowns/accounting-category-consults.service';
import { AccountingConceptConsultsService } from '../../../services/dropdowns/accounting-concept-consults.service';
import { HouseConsultsService } from '../../../services/dropdowns/house-consults.service';
import { VehicleConsultsService } from '../../../services/dropdowns/vehicle-consults.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { UserService } from '../../../services/users/user.service';
import { AccountingCategories } from 'src/app/models/dropdowns/accounting-categories';
import { AccountingConcepts } from 'src/app/models/dropdowns/accounting-concepts';
import { Houses } from 'src/app/models/dropdowns/houses';
import { Vehicles } from 'src/app/models/dropdowns/vehicles';
import { ARIA_DESCRIBER_PROVIDER_FACTORY } from '@angular/cdk/a11y';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import { MatCalendarCellCssClasses } from '@angular/material';


export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-accounting-activity-form',
  templateUrl: './accounting-activity-form.component.html',
  styleUrls: ['./accounting-activity-form.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class AccountingActivityFormComponent implements OnInit {
  userId: string;
  keyBackUp: string;
  userIdBackup: string;
  categoriesList: AccountingCategories[];
  conceptsList: AccountingConcepts[];
  housesList: Houses[];
  vehiclesList: Vehicles[];
  formType: string;
  conceptsInitialized: boolean = false;
  
  

  constructor(public dialogRef: MatDialogRef<AccountingActivityFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public activityConsultsService: AccountingActivityConsultsService,
    public categoryConsultsService: AccountingCategoryConsultsService,
    public conceptConsultsService: AccountingConceptConsultsService,
    public houseConsultsService: HouseConsultsService,
    public vehicleConsultsService: VehicleConsultsService,
    private toastrService: ToastrService,
    private userService: UserService,
    private dateAdapter: DateAdapter<Date>,
    ) { 
      this.formType = this.data['type'];
      this.activityConsultsService.selectedActivity.type = this.data['type'];
    }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.getCategories();
        this.getConcepts(this.activityConsultsService.selectedActivity.category);
        this.getHouses();
        this.getVehicles();
      } 
    });
    this.dateAdapter.getFirstDayOfWeek = () => { return 1; }
  }


  onSubmit(activityForm: NgForm){
    if (activityForm.value.date["_d"]){
      activityForm.value.date = activityForm.value.date["_d"].toISOString();
    }
    if(this.activityConsultsService.selectedActivity.$key == null){
      this.activityConsultsService.addActivity(activityForm.value);
      this.toastrService.success('Operación ejecutada con exito','Añadir movimiento')
      this.dialogRef.close();
    }else{
      this.activityConsultsService.updateActivity(activityForm.value);
      this.toastrService.success('Operación ejecutada con exito','Editar movimiento')
      this.dialogRef.close();
    }
  }

  resetForm(activityForm: NgForm){
    this.keyBackUp = activityForm.value.$key;
    this.userIdBackup = activityForm.value.userId;
    if (activityForm != null){
      activityForm.reset();
    }
    activityForm.value.$key = this.keyBackUp;
    activityForm.value.userId = this.userIdBackup;
  }

  getCategories(){
    var content = this.categoryConsultsService.getCategories(this.userId);
    content.snapshotChanges().subscribe(categories => {
      this.categoriesList = [];
      categories.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        if(register["type"] == this.data['type']){
          this.categoriesList.push(register as AccountingCategories)
        }
      });
    });
  }
  getConcepts(category:string){
    if (this.conceptsInitialized){
      this.activityConsultsService.selectedActivity.concept = null;
      this.activityConsultsService.selectedActivity.vehicle = null;
      this.activityConsultsService.selectedActivity.house = null;
    }
    var content = this.conceptConsultsService.getConcepts(this.userId);
    content.snapshotChanges().subscribe(concepts => {
      this.conceptsList = [];
      concepts.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        if(register["category"] == category){
          this.conceptsList.push(register as AccountingConcepts)
        }
      });
    });
    this.conceptsInitialized = true;
  }
  emptyVehicle(){
    this.activityConsultsService.selectedActivity.milometer = null;
    this.activityConsultsService.selectedActivity.litres = null;
  }

  getHouses(){
    var content = this.houseConsultsService.getHouses(this.userId);
    content.snapshotChanges().subscribe(houses => {
      this.housesList = [];
      houses.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.housesList.push(register as Houses)
      });
    });
  }
  getVehicles(){
    var content = this.vehicleConsultsService.getVehicles(this.userId);
    content.snapshotChanges().subscribe(vehicles => {
      this.vehiclesList = [];
      vehicles.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.vehiclesList.push(register as Vehicles)
      });
    });
  }
  dateClass() {
    return (d: Date): MatCalendarCellCssClasses => {
      const day = d['_d'].getDay();
      if(day == 0 || day ==6){
        return 'weekend'
      }
    };
  }
}
