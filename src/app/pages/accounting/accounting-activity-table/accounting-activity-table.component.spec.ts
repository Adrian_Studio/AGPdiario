import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingActivityTableComponent } from './accounting-activity-table.component';

describe('AccountingActivityTableComponent', () => {
  let component: AccountingActivityTableComponent;
  let fixture: ComponentFixture<AccountingActivityTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingActivityTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingActivityTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
