import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges, SimpleChange, HostListener } from '@angular/core';
import { AccountingActivityConsultsService } from '../../../services/accounting/accounting-activity-consults.service';
import { AccountingActivity } from '../../../models/accounting/accounting-activity';
import { AccountingCategories } from '../../../models/dropdowns/accounting-categories';
import { AccountingConcepts } from '../../../models/dropdowns/accounting-concepts';
import { ToastrService } from 'ngx-toastr';
import { AccountingActivityFormComponent } from "../accounting-activity-form/accounting-activity-form.component";
import { MatDialog } from '@angular/material';
import { Subject, concat } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { AccountingFilterService } from '../../../services/accounting/accounting-filter.service';
import { AccountingFilter } from '../../../models/accounting/accounting-filter';

@Component({
  selector: 'app-accounting-activity-table',
  templateUrl: './accounting-activity-table.component.html',
  styleUrls: ['./accounting-activity-table.component.scss'],
})
export class AccountingActivityTableComponent implements OnInit, OnChanges {
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @Input() activityListUncut: AccountingActivity[];
  @Input() categoriesListUncut: AccountingCategories[];
  @Input() conceptsListUncut: AccountingConcepts[];
  @HostListener('window:resize', ['$event']) 
  onResize(event){
    var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
    $('div.dataTables_scrollBody').height( scrollY );   
  }
  activityListTable: AccountingActivity[]; // Variable necesaría para el correcto renderizado de la tabla
  dtOptions: any = { };
  dtTrigger: Subject<any> = new Subject(); // Variable necesaría para reescribir la tabla
  cutTable: boolean = false;
  tableLenght: number;
 


  constructor(
      private activityConsultsService: AccountingActivityConsultsService,
      private toastrService: ToastrService,
      public dialog: MatDialog,
      public accountingFilterService: AccountingFilterService,
    ) {  }

  ngOnInit() {
    this.datatableOptions();
    if(this.activityListUncut){
      this.renderTable();
    }
  }

  ngOnChanges(changes: SimpleChanges){
    $('.table-responsive table').addClass('hidden');
    $('.cutTable').addClass('hidden');
    $('.loading').removeClass('hidden');
    if (this.activityListUncut){
      if (this.dtElement.dtInstance){
        this.renderTable();
      } else{
        if(this.activityListUncut.length >= 100){
          this.activityListTable = this.activityListUncut.slice(0,100);
          this.cutTable = true;
          this.tableLenght = this.activityListUncut.length;
        }else{
          this.activityListTable = this.activityListUncut;
          this.cutTable = false; 
        }
        this.dtTrigger.next();
      }
    }
  }

 

  datatableOptions(){
    this.dtOptions = {
      responsive: true,
      scrollY: (window.innerHeight - 20),
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      order: [[ 0, "desc" ], [ 1, "asc" ], [ 2, "asc" ]],
      columnDefs: [
        {"targets":0, "type":"date-eu"},
        { responsivePriority: 0, targets: 0 },
        { responsivePriority: 1, targets: 5 },
        { responsivePriority: 2, targets: 3 },
        { responsivePriority: 3, targets: 2 },
        { responsivePriority: 4, targets: 1 },
        { responsivePriority: 5, targets: 4 },
      ],
      initComplete: function(settings, json) {
        $('.loading').addClass('hidden');
        $('.table-responsive table').removeClass('hidden');
        $('.cutTable').removeClass('hidden');
        var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
        $('div.dataTables_scrollBody').height( scrollY );        
      }
    };
  }
  
  renderTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.cutTable = false;
      $('.table-responsive table').addClass('hidden');
      $('.cutTable').addClass('hidden');
      $('.loading').removeClass('hidden');
      if(this.accountingFilterService.activeFilter){
        this.applyFilter();
        if(this.activityListTable.length >= 100){
          this.tableLenght = this.activityListTable.length;
          this.activityListTable = this.activityListTable.slice(0,100);
          this.cutTable = true;
        }else{
          this.activityListTable = this.activityListTable;
          this.cutTable = false; 
        }
      }else{
        if(this.activityListUncut.length >= 100){
          this.activityListTable = this.activityListUncut.slice(0,100);
          this.cutTable = true;
          this.tableLenght = this.activityListUncut.length;
        }else{
          this.activityListTable = this.activityListUncut;
          this.cutTable = false;
        }
      }
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe(); 
  }

  copyActivity(activity: AccountingActivity){
    this.activityConsultsService.initializeForm();
    const dialogRef = this.dialog.open(AccountingActivityFormComponent, {
      width: '540px',
      panelClass: activity.type,
      data: {
        type: activity.type,
      }
    });
    
    dialogRef.afterClosed().subscribe(result => {
      this.activityConsultsService.initializeForm();
    });
    this.activityConsultsService.selectedActivity = Object.assign({},activity)
    this.activityConsultsService.selectedActivity.$key = null;
    this.activityConsultsService.selectedActivity.date = null;
    this.activityConsultsService.selectedActivity.category = this.activityConsultsService.selectedActivity.categoryKey;
    this.activityConsultsService.selectedActivity.concept = this.activityConsultsService.selectedActivity.conceptKey;
 
  }

  editActivity(activity: AccountingActivity){
    const dialogRef = this.dialog.open(AccountingActivityFormComponent, {
      width: '540px',
      panelClass: activity.type,
      data: {
        type: activity.type,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.activityConsultsService.initializeForm();
    });
    this.activityConsultsService.selectedActivity = Object.assign({},activity);
    this.activityConsultsService.selectedActivity.category = this.activityConsultsService.selectedActivity.categoryKey;
    this.activityConsultsService.selectedActivity.concept = this.activityConsultsService.selectedActivity.conceptKey;   
  }

  deleteActivity(activity: AccountingActivity){
    if(confirm("¿Esta seguro de que quiere eliminar el registro?") == true){
      this.activityConsultsService.deleteActivity(activity);
      this.toastrService.success('Operación ejecutada con exito','Eliminar movimiento');
    }
  }

  applyFilter(){
    let filter: AccountingFilter = this.accountingFilterService.appliedFilter;
    let deletingList: AccountingActivity[];
    let filteredList: AccountingActivity[] = this.activityListUncut;
    if (filter.type){
      let emptyType:boolean = true; 
      deletingList = filteredList;
      filter.type.forEach(type => {
        deletingList = deletingList.filter(element => element.type != type);
        emptyType = false;
      })
      if (emptyType){
        filter.type = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.category){     
      let emptyCategory:boolean = true; 
      deletingList = filteredList;
      filter.category.forEach(category => {
        deletingList = deletingList.filter(element => element.categoryKey != category);
        emptyCategory = false
      })
      if (emptyCategory){
        filter.category = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.concept){
      let emptyConcept:boolean = true;
      deletingList = filteredList;
      filter.concept.forEach(concept => {
        deletingList = deletingList.filter(element => element.conceptKey != concept);
        emptyConcept = false;
      })
      if (emptyConcept){
        filter.concept = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.house){
      let emptyHouse:boolean = true;
      deletingList = filteredList;
      filter.house.forEach(house => {
        deletingList = deletingList.filter(element => element.house != house);
        emptyHouse = false;
      })
      if (emptyHouse){
        filter.house = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.vehicle){
      let emptyVehicle:boolean = true;
      deletingList = filteredList;
      filter.vehicle.forEach(vehicle => {
        deletingList = deletingList.filter(element => element.vehicle != vehicle);
        emptyVehicle = false;
      })
      if (emptyVehicle){
        filter.vehicle = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.year){
      let emptyYear:boolean = true;
      deletingList = filteredList;
      filter.year.forEach(year => {
        deletingList = deletingList.filter(element =>{ 
          var date = new Date(element.date);
          var elementYear = date.getFullYear();
          return elementYear != +year
        });
        emptyYear = false;
      })
      if (emptyYear){
        filter.year = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.month){
      let emptyMonth:boolean = true;
      deletingList = filteredList;
      filter.month.forEach(month => {
        deletingList = deletingList.filter(element =>{ 
          var date = new Date(element.date);
          var elementMonth = date.getMonth();
          return elementMonth != +month
        });
        emptyMonth = false;
      })
      if (emptyMonth){
        filter.month = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }

    if (filter.startDate){
      deletingList = filteredList;
      if(filter.startDate["_d"]){
        filter.startDate = filter.startDate["_d"].toISOString();
      }
      deletingList = deletingList.filter(element => element.date < filter.startDate)
      filteredList = filteredList.filter(x => !deletingList.includes(x));
    }
    if (filter.endDate){
      deletingList = filteredList;
      if(filter.endDate["_d"]){
        filter.endDate = filter.endDate["_d"].toISOString();
      }
      deletingList = deletingList.filter(element => element.date > filter.endDate)
      filteredList = filteredList.filter(x => !deletingList.includes(x));
    }

    if (!filter.type && !filter.category && !filter.concept && !filter.year && !filter.month && !filter.startDate && !filter.house && !filter.vehicle){
      filteredList = this.activityListUncut;
    }
    this.activityListTable = filteredList;
  }
}
