import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingAnnualSummaryComponent } from './accounting-annual-summary.component';

describe('AccountingAnnualSummaryComponent', () => {
  let component: AccountingAnnualSummaryComponent;
  let fixture: ComponentFixture<AccountingAnnualSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingAnnualSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingAnnualSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
