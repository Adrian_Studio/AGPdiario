import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges, SimpleChange, HostListener } from '@angular/core';
import { AccountingActivity } from '../../../models/accounting/accounting-activity';
import { AccountingCategories } from '../../../models/dropdowns/accounting-categories';
import { AccountingConcepts } from '../../../models/dropdowns/accounting-concepts';
import { MatDialog } from '@angular/material';
import { Subject, concat } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { AccountingFilterService } from '../../../services/accounting/accounting-filter.service';
import { AccountingFilter } from '../../../models/accounting/accounting-filter';

@Component({
  selector: 'app-accounting-annual-summary',
  templateUrl: './accounting-annual-summary.component.html',
  styleUrls: ['./accounting-annual-summary.component.scss'],
})
export class AccountingAnnualSummaryComponent implements OnInit, OnChanges {
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @Input() activityListUncut: AccountingActivity[];
  @Input() categoriesListUncut: AccountingCategories[];
  @Input() conceptsListUncut: AccountingConcepts[];
  @HostListener('window:resize', ['$event']) 
  onResize(event){
    var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
    $("div.dataTables_scrollBody").css("max-height", scrollY);
  }
  dtOptions: any = { };
  dtTrigger: Subject<any> = new Subject();
  table_columns:any;
  table_data:any;

  constructor(
      public dialog: MatDialog,
      public accountingFilterService: AccountingFilterService,
    ) { }

  ngOnInit() {
    this.datatableOptions();
    if(this.activityListUncut){
      this.renderTable();
    }
  }

  ngOnChanges(changes: SimpleChanges){
    $('.table-responsive table').addClass('hidden');
    $('.loading').removeClass('hidden');
    if (this.activityListUncut){
      if(this.table_columns && this.table_data){
        if (this.dtElement.dtInstance){
          this.renderTable();
        } else{
          //No debería entrar nunca pero se ha dejado para que se cree la instancia en caso de que no la hubiese
          this.dtTrigger.next();
        }
      } else{
        this.applyFilter();
        this.dtTrigger.next();
      }
    } 
  }

  datatableOptions(){
    this.dtOptions = {
      responsive:true,
      scrollY: '1000px',
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      ordering: false,
      searching: false,
      info: false,
      columnDefs: [
        {"targets":1, "type":"date-eu"},
      ],
      initComplete: function(settings, json) {
        $('.table-responsive table').removeClass('hidden');
        $('.loading').addClass('hidden');
        var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
        $("div.dataTables_scrollBody").css("max-height", scrollY);
      }
    };
  }

  renderTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      $('.table-responsive table').addClass('hidden');
      $('.loading').removeClass('hidden');
      // Destroy the table first
      dtInstance.destroy();
      // Asociacion de los nuevos datos de la variable
      this.applyFilter();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  processData(data:any){
    let firstYear:number;
    let lastYear:number;
    this.table_columns = [];
    this.table_data = [];
    data.sort((a, b) => {
      if(a.type === b.type){
        if(a.category === b.category){
          return a.concept.localeCompare(b.concept);
        }else{
          return a.category.localeCompare(b.category);
        }
      }else{
        return b.type.localeCompare(a.type);
      }
    })
    data.forEach( activity => {
      
      let date = new Date(activity.date);
      let year = date.getFullYear()
      if(!this.table_columns.includes(year)){
        this.table_columns.push(year);
      }
      if(firstYear == undefined || firstYear > year){
        firstYear = year ; 
      }
      if(lastYear == undefined || lastYear < year){
        lastYear = year;
      }
      if(!(this.table_data.find(data => data.Grupo === 'Balance'))){
        let index = this.table_data.length;
        this.table_data[index] =[];
        this.table_data[index].Grupo = 'Balance';
        this.table_data[index].type = 'Balance';
      }

      if(!(this.table_data.find(data => data.Grupo == activity.type + 's'))){
        let index = this.table_data.length;
        this.table_data[index] =[];
        this.table_data[index].Grupo = activity.type + 's';
        this.table_data[index].type = activity.type;
      }
      if(!(this.table_data.find(data => data.Grupo === activity.category))){
        let index = this.table_data.length;
        this.table_data[index] =[];
        this.table_data[index].Grupo = activity.category;
        this.table_data[index].type = 'Categoria';
      }
      if(!(this.table_data.find(data => data.Grupo === activity.concept && data.CategoriaGrupo === activity.category))){
        let index = this.table_data.length;
        this.table_data[index] =[];
        this.table_data[index].Grupo = activity.concept;
        this.table_data[index].CategoriaGrupo = activity.category;
        this.table_data[index].type = 'Concepto';
      }
      if(!this.table_data.find(data => data.Grupo === 'Balance').Total){
        this.table_data.find(data => data.Grupo === 'Balance').Total = null;
      }
      if(!this.table_data.find(data => data.Grupo === 'Balance')[year]){
        this.table_data.find(data => data.Grupo === 'Balance')[year] = null;
      }
      if(activity.type == 'Ingreso'){        
        this.table_data.find(data => data.Grupo === 'Balance').Total += activity.value;
        this.table_data.find(data => data.Grupo === 'Balance')[year] += activity.value;
      }else if(activity.type == 'Gasto'){
        this.table_data.find(data => data.Grupo === 'Balance').Total -= activity.value;
        this.table_data.find(data => data.Grupo === 'Balance')[year] -= activity.value;
      }
      if(!this.table_data.find(data => data.Grupo == activity.type + 's').Total){
        this.table_data.find(data => data.Grupo == activity.type + 's').Total = null;
      }
      if(!this.table_data.find(data => data.Grupo == activity.type + 's')[year]){
        this.table_data.find(data => data.Grupo == activity.type + 's')[year] = null;
      }
      if(!this.table_data.find(data => data.Grupo == activity.type + 's').Total){
        this.table_data.find(data => data.Grupo == activity.type + 's').Total = null;
      }
      this.table_data.find(data => data.Grupo == activity.type + 's').Total += activity.value;
      this.table_data.find(data => data.Grupo == activity.type + 's')[year] += activity.value;

      if(!this.table_data.find(data => data.Grupo === activity.category).Total){
        this.table_data.find(data => data.Grupo === activity.category).Total = null;
      }
      if(!this.table_data.find(data => data.Grupo === activity.category)[year]){
        this.table_data.find(data => data.Grupo === activity.category)[year] = null;
      }
      if(!this.table_data.find(data => data.Grupo === activity.category).Total){
        this.table_data.find(data => data.Grupo === activity.category).Total = null;
      }
      this.table_data.find(data => data.Grupo === activity.category).Total += activity.value;
      this.table_data.find(data => data.Grupo === activity.category)[year] += activity.value;

      if(!this.table_data.find(data => data.Grupo === activity.concept && data.CategoriaGrupo === activity.category).Total){
        this.table_data.find(data => data.Grupo === activity.concept && data.CategoriaGrupo === activity.category).Total = null;
      }
      if(!this.table_data.find(data => data.Grupo === activity.concept && data.CategoriaGrupo === activity.category)[year]){
        this.table_data.find(data => data.Grupo === activity.concept && data.CategoriaGrupo === activity.category)[year] = null;
      }
      if(!this.table_data.find(data => data.Grupo === activity.concept && data.CategoriaGrupo === activity.category).Total){
        this.table_data.find(data => data.Grupo === activity.concept && data.CategoriaGrupo === activity.category).Total = null;
      }
      this.table_data.find(data => data.Grupo === activity.concept && data.CategoriaGrupo === activity.category).Total += activity.value;
      this.table_data.find(data => data.Grupo === activity.concept && data.CategoriaGrupo === activity.category)[year] += activity.value;
    });
    this.table_columns.sort();
    this.table_columns.push('Media');
    this.table_columns.push('Total');
    this.table_data.forEach(element => {
      element.Media = element.Total / ((lastYear - firstYear) + 1)
    });
    //this.table_columns.push('Grupo');
    this.table_columns.reverse();
  }
  

  applyFilter(){
    let filter: AccountingFilter = this.accountingFilterService.appliedFilter;
    let deletingList: AccountingActivity[];
    let filteredList: AccountingActivity[] = this.activityListUncut;
    if (filter.type){
      let emptyType:boolean = true; 
      deletingList = filteredList;
      filter.type.forEach(type => {
        deletingList = deletingList.filter(element => element.type != type);
        emptyType = false;
      })
      if (emptyType){
        filter.type = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.category){     
      let emptyCategory:boolean = true; 
      deletingList = filteredList;
      filter.category.forEach(category => {
        deletingList = deletingList.filter(element => element.categoryKey != category);
        emptyCategory = false
      })
      if (emptyCategory){
        filter.category = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.concept){
      let emptyConcept:boolean = true;
      deletingList = filteredList;
      filter.concept.forEach(concept => {
        deletingList = deletingList.filter(element => element.conceptKey != concept);
        emptyConcept = false;
      })
      if (emptyConcept){
        filter.concept = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.house){
      let emptyHouse:boolean = true;
      deletingList = filteredList;
      filter.house.forEach(house => {
        deletingList = deletingList.filter(element => element.house != house);
        emptyHouse = false;
      })
      if (emptyHouse){
        filter.house = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.vehicle){
      let emptyVehicle:boolean = true;
      deletingList = filteredList;
      filter.vehicle.forEach(vehicle => {
        deletingList = deletingList.filter(element => element.vehicle != vehicle);
        emptyVehicle = false;
      })
      if (emptyVehicle){
        filter.vehicle = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.year){
      let emptyYear:boolean = true;
      deletingList = filteredList;
      filter.year.forEach(year => {
        deletingList = deletingList.filter(element =>{ 
          var date = new Date(element.date);
          var elementYear = date.getFullYear();
          return elementYear != +year
        });
        emptyYear = false;
      })
      if (emptyYear){
        filter.year = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.month){
      let emptyMonth:boolean = true;
      deletingList = filteredList;
      filter.month.forEach(month => {
        deletingList = deletingList.filter(element =>{ 
          var date = new Date(element.date);
          var elementMonth = date.getMonth();
          return elementMonth != +month
        });
        emptyMonth = false;
      })
      if (emptyMonth){
        filter.month = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }

    if (filter.startDate){
      deletingList = filteredList;
      if(filter.startDate["_d"]){
        filter.startDate = filter.startDate["_d"].toISOString();
      }
      deletingList = deletingList.filter(element => element.date < filter.startDate)
      filteredList = filteredList.filter(x => !deletingList.includes(x));
    }
    if (filter.endDate){
      deletingList = filteredList;
      if(filter.endDate["_d"]){
        filter.endDate = filter.endDate["_d"].toISOString();
      }
      deletingList = deletingList.filter(element => element.date > filter.endDate)
      filteredList = filteredList.filter(x => !deletingList.includes(x));
    }

    if (!filter.type && !filter.category && !filter.concept && !filter.year && !filter.month && !filter.startDate && !filter.house && !filter.vehicle){
      filteredList = this.activityListUncut;
    }
    this.processData(filteredList);
  }
}
