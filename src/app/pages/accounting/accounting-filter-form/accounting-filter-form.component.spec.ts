import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingFilterFormComponent } from './accounting-filter-form.component';

describe('AccountingFilterFormComponent', () => {
  let component: AccountingFilterFormComponent;
  let fixture: ComponentFixture<AccountingFilterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingFilterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
