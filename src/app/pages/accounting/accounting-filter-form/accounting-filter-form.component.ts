import { Component, OnInit, Inject, ChangeDetectorRef} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { AccountingCategoryConsultsService } from '../../../services/dropdowns/accounting-category-consults.service';
import { AccountingConceptConsultsService } from '../../../services/dropdowns/accounting-concept-consults.service';
import { HouseConsultsService } from '../../../services/dropdowns/house-consults.service';
import { VehicleConsultsService } from '../../../services/dropdowns/vehicle-consults.service';
import { NgForm } from '@angular/forms';
import { UserService } from '../../../services/users/user.service';
import { AccountingFilterService } from '../../../services/accounting/accounting-filter.service';
import { AccountingCategories } from 'src/app/models/dropdowns/accounting-categories';
import { AccountingConcepts } from 'src/app/models/dropdowns/accounting-concepts';
import { Houses } from 'src/app/models/dropdowns/houses';
import { Vehicles } from 'src/app/models/dropdowns/vehicles';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MatCalendarCellCssClasses } from '@angular/material';


export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-accounting-filter-form',
  templateUrl: './accounting-filter-form.component.html',
  styleUrls: ['./accounting-filter-form.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class AccountingFilterFormComponent implements OnInit {
  userId: string;
  categoriesList: AccountingCategories[];
  conceptsList: AccountingConcepts[];
  housesList: Houses[];
  vehiclesList: Vehicles[];
  monthsList: any[] = [{"name": '01 - Enero', "number": 0},
  {"name": '02 - Febrero', "number": 1},
  {"name": '03 - Marzo', "number": 2},
  {"name": '04 - Abril', "number": 3},
  {"name": '05 - Mayo', "number": 4},
  {"name": '06 - Junio', "number": 5},
  {"name": '07 - Julio', "number": 6},
  {"name": '08 - Agosto', "number": 7},
  {"name": '09 - Septiembre', "number": 8},
  {"name": '10 - Octubre', "number": 9},
  {"name": '11 - Noviembre', "number": 10},
  {"name": '12 - Diciembre', "number": 11}];
  yearsList: number[];
  smallWindow = window.innerWidth < 400;
  selectedPage: string;

  constructor(public dialogRef: MatDialogRef<AccountingFilterFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public categoryConsultsService: AccountingCategoryConsultsService,
    public conceptConsultsService: AccountingConceptConsultsService,
    public houseConsultsService: HouseConsultsService,
    public vehicleConsultsService: VehicleConsultsService,
    private userService: UserService,
    public filterService: AccountingFilterService,
    private dateAdapter: DateAdapter<Date>,
    ) { 
      this.yearsList = this.data["yearsList"];
      this.selectedPage = this.data["selectedPage"];
    }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.getCategories();
        this.getConcepts();
        this.getHouses();
        this.getVehicles();
        this.formManagement();
      } 
    });
    this.dateAdapter.getFirstDayOfWeek = () => { return 1; }
  }

  resetForm(filterForm: NgForm){
    if (filterForm != null){
      filterForm.reset();
    }
  }
  getSavedFilter(){
    this.filterService.selectSavedFilter();
  }

  onSubmit(filterForm: NgForm){
    
    this.filterService.selectedFilter = filterForm.value
    this.filterService.applyFilter();
    this.dialogRef.close();
  }

  getCategories(){
    var content = this.categoryConsultsService.getCategories(this.userId);
    content.snapshotChanges().subscribe(categories => {
      this.categoriesList = [];
      categories.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.categoriesList.push(register as AccountingCategories)
      });
    });
  }
  getConcepts(){
    var content = this.conceptConsultsService.getConcepts(this.userId);
    content.snapshotChanges().subscribe(concepts => {
      this.conceptsList = [];
      concepts.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.conceptsList.push(register as AccountingConcepts)
      });
    });
  }

  getHouses(){
    var content = this.houseConsultsService.getHouses(this.userId);
    content.snapshotChanges().subscribe(houses => {
      this.housesList = [];
      houses.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.housesList.push(register as Houses)
      });
    });
  }
  getVehicles(){
    var content = this.vehicleConsultsService.getVehicles(this.userId);
    content.snapshotChanges().subscribe(vehicles => {
      this.vehiclesList = [];
      vehicles.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.vehiclesList.push(register as Vehicles)
      });
    });
  }
  formManagement(){
    switch (this.selectedPage) {
      case 'accountingTable':
        /*if(typeof this.filterService.selectedFilter.year != "object"){
          this.filterService.selectedFilter.year = null;
        }*/
        break;
      case 'accountingAnnualSummary':
        /*this.filterService.selectedFilter.concept = null;
        this.filterService.selectedFilter.month = null;
        this.filterService.selectedFilter.year = null;
        this.filterService.selectedFilter.startDate = null;
        this.filterService.selectedFilter.endDate = null;*/
        break;
      case 'accountingMonthlySummary':
        /*this.filterService.selectedFilter.concept = null;
        this.filterService.selectedFilter.month = null;*/
        var today = new Date();
        if(!this.filterService.selectedFilter.year){
          this.filterService.selectedFilter.year = today.getFullYear();
        }else{
          this.filterService.selectedFilter.year = this.filterService.selectedFilter.year[0];
        }
        
        /*this.filterService.selectedFilter.startDate = null;
        this.filterService.selectedFilter.endDate = null;*/
        break;
      case 'accountingGraphs':
        this.filterService.selectedFilter.concept = null;
        break;        
      default:
        break;
    }
  }
  dateClass() {
    return (d: Date): MatCalendarCellCssClasses => {
      const day = d['_d'].getDay();
      if(day == 0 || day ==6){
        return 'weekend'
      }
    };
  }
}
