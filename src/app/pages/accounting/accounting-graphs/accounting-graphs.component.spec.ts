import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingGraphsComponent } from './accounting-graphs.component';

describe('AccountingGraphsComponent', () => {
  let component: AccountingGraphsComponent;
  let fixture: ComponentFixture<AccountingGraphsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingGraphsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingGraphsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
