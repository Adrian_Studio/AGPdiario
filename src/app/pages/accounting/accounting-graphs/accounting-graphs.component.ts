import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange, HostListener } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, Color } from 'ng2-charts';
import { AccountingActivity } from '../../../models/accounting/accounting-activity';
import { AccountingCategories } from '../../../models/dropdowns/accounting-categories';
import { AccountingConcepts } from '../../../models/dropdowns/accounting-concepts';
import { AccountingFilterService } from '../../../services/accounting/accounting-filter.service';
import { AccountingFilter } from '../../../models/accounting/accounting-filter';

@Component({
  selector: 'app-accounting-graphs',
  templateUrl: './accounting-graphs.component.html',
  styleUrls: ['./accounting-graphs.component.scss']
})
export class AccountingGraphsComponent implements OnInit, OnChanges  {
  @Input() activityListUncut: AccountingActivity[];
  @Input() categoriesListUncut: AccountingCategories[];
  @Input() conceptsListUncut: AccountingConcepts[];
  @HostListener('window:resize', ['$event']) 
  onResize(event){
    if(window.innerWidth > 1200){
      this.showLabels = true;
    }else{
      this.showLabels = false;
    }
  }
  activityList: AccountingActivity[];
  balanceChartLabels: Label[] = ['1/1/2019','1/1/2020'];
  balanceChartData: any[] = [
    { data: [], label: 'Ingresos', pointRadius: 3, showLine: false},
    { data: [], label: 'Gastos', pointRadius: 3, showLine: false},
    { data: [], label: 'Balance', pointRadius: 1, lineTension: 0.1}
  ];
  balanceChartType: ChartType = 'line';
  balanceChartOptions: any = {
    //responsive: true,
    tooltips: {
      mode: 'index',
      callbacks: {
        label: function(tooltipItem, data) {
          return data.datasets[tooltipItem.datasetIndex].label +': ' +tooltipItem.yLabel.toLocaleString("es",{style:"currency", currency:"EUR"});
        }
      },
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
        type: 'time',
        time: {
          displayFormats: {
            quarter: 'MMM YYYY'
        }
        }
      }],
      yAxes: [{
        //type: 'logarithmic',
        type: 'linear',
        //stacked: true,
        ticks: {
            beginAtZero: true,
            userCallback: function(tick) {
              return tick.toLocaleString("es",{style:"decimal", maximumFractionDigits: 1}) + " €";
            },
        },
        scaleLabel: {
            //labelString: 'Voltage',
            //display: false
        }
      }],
    },
  };
  balanceChartColors: Color[] = [
    { // green
      backgroundColor: 'rgba(0,255,0,0.2)',
      borderColor: 'rgba(0,255,0,1)',
      pointBackgroundColor: 'rgba(0,255,0,1)',
      pointBorderColor: 'rgba(0,255,0,0.2)',
      pointHoverBackgroundColor: 'rgba(0,255,0,0.2)',
      pointHoverBorderColor: 'rgba(0,255,0,0.8)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.2)',
      borderColor: 'rgba(255,0,0,1)',
      pointBackgroundColor: 'rgba(255,0,0,1)',
      pointBorderColor: 'rgba(255,0,0,0.2)',
      pointHoverBackgroundColor: 'rgba(255,0,0,0.2)',
      pointHoverBorderColor: 'rgba(255,0,0,0.8)'
    },
    { // orange
      backgroundColor: 'rgba(255,165,0,0.2)',
      borderColor: 'rgba(255,165,0,1)',
      pointBackgroundColor: 'rgba(255,165,0,1)',
      pointBorderColor: 'rgba(255,165,0,0.2)',
      pointHoverBackgroundColor: 'rgba(255,165,0,0.2)',
      pointHoverBorderColor: 'rgba(255,165,0,0.8)'
    }];
  categoryIncomeChartLabels: Label[] = [];
  categoryIncomeChartData: SingleDataSet = [,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,];
  categoryExpenseChartLabels: Label[] = [];
  categoryExpenseChartData: SingleDataSet = [,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,];
  conceptIncomeChartLabels: Label[] = [];
  conceptIncomeChartData: number[] = [,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,]; // Tiene que ser este tipo para que no de problemas el .push
  conceptExpenseChartLabels: Label[] = [];
  conceptExpenseChartData: number[] = [,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,]; // Tiene que ser este tipo para que no de problemas el .push
  pieChartType: ChartType = 'doughnut';
  pieChartOptions: ChartOptions = {
    tooltips: {
      callbacks: {
        label: function(tooltipItem, data) {
          return data.labels[tooltipItem.index] +': ' + data.datasets[0].data[tooltipItem.index].toLocaleString("es",{style:"currency", currency:"EUR"});
        },

      },
    },
  };
  conceptsIncomeList: any[];
  conceptsExpenseList: any[];
  totalCategoryIncome: number;
  totalConceptIncome: number;
  totalCategoryExpense: number;
  totalConceptExpense: number;
  balance: number;
  conceptIncomeChartTitle: string;
  conceptExpenseChartTitle: string;
  showLabels: boolean = false;

  constructor(      
    public accountingFilterService: AccountingFilterService,
    ) { }

  ngOnInit() {
    if(window.innerWidth > 1200){
      this.showLabels = true;
    }
  }
  
  ngOnChanges(changes: SimpleChanges){
    if(this.activityListUncut){
      this.applyFilter();
    }
  }

  processPieChartData(data:any){
    let categoryIncomeChartLabels = [];
    let categoryExpenseChartLabels = [];
    this.categoryIncomeChartData = [];
    this.categoryExpenseChartData = [];
    this.conceptsIncomeList = [];
    this.conceptsExpenseList = [];
    this.totalCategoryIncome = 0;
    this.totalCategoryExpense = 0;
    data.forEach( activity => {
      if(activity.type == 'Ingreso'){
        if(!(categoryIncomeChartLabels.find(incomeLabel => incomeLabel === activity.categoryKey))){
          let index = categoryIncomeChartLabels.length;
          categoryIncomeChartLabels[index] = activity.categoryKey;
        }
        if(!(this.conceptsIncomeList.find(label => label.concept === activity.concept))){
          this.conceptsIncomeList.push({"category": activity.category, "concept": activity.concept, "value": activity.value});
        }else{
          this.conceptsIncomeList.find(label => label.concept === activity.concept).value += activity.value;
        }
        let categoryIncomeIndex = categoryIncomeChartLabels.indexOf(activity.categoryKey);
        if(!this.categoryIncomeChartData[categoryIncomeIndex]){
          this.categoryIncomeChartData[categoryIncomeIndex] = activity.value;
          this.totalCategoryIncome += activity.value;
        } else{
          this.categoryIncomeChartData[categoryIncomeIndex] += activity.value;
          this.totalCategoryIncome += activity.value;
        }
      }else if(activity.type == 'Gasto'){
        if(!(categoryExpenseChartLabels.find(expenseLabel => expenseLabel === activity.categoryKey))){
          let index = categoryExpenseChartLabels.length;
          categoryExpenseChartLabels[index] = activity.categoryKey;
        }
        if(!(this.conceptsExpenseList.find(label => label.concept === activity.concept))){
          this.conceptsExpenseList.push({"category": activity.category, "concept": activity.concept, "value": activity.value});
        }else{
          this.conceptsExpenseList.find(label => label.concept === activity.concept).value += activity.value;
        }
        let categoryExpenseIndex = categoryExpenseChartLabels.indexOf(activity.categoryKey);
        if(!this.categoryExpenseChartData[categoryExpenseIndex]){
          this.categoryExpenseChartData[categoryExpenseIndex] = activity.value;
          this.totalCategoryExpense += activity.value;
        } else{
          this.categoryExpenseChartData[categoryExpenseIndex] += activity.value;
          this.totalCategoryExpense += activity.value;
        }
      }
    });
    this.categoryIncomeChartLabels = [];
    this.categoryExpenseChartLabels = [];
    let categoryIncome: any;
    let categoryExpense: any;
    categoryIncomeChartLabels.forEach(element => {
      categoryIncome = this.categoriesListUncut.find(category => category.$key === element).category;
      this.categoryIncomeChartLabels.push(categoryIncome)
    });
    categoryExpenseChartLabels.forEach(element => {
      categoryExpense = this.categoriesListUncut.find(category => category.$key === element).category;
      this.categoryExpenseChartLabels.push(categoryExpense)
    });
  }


  public incomeChartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    if(active[0]){
      this.conceptIncomeChartLabels = [];
      this.conceptIncomeChartData = [];
      this.totalConceptIncome = 0;
      this.conceptsIncomeList.filter(concept =>concept.category === active[0]["_view"].label).forEach(element => {
        this.conceptIncomeChartLabels.push(element.concept); 
        this.conceptIncomeChartData.push(element.value);
        this.totalConceptIncome += element.value;
      });
      for (let index = 0; index < 20; index++) {  //Introducir valores nulos para que genere los colores
        this.conceptIncomeChartData.push(null);
      }
      this.conceptIncomeChartTitle = active[0]["_view"].label;
    }
  }
  public expenseChartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    if(active[0]){
      this.conceptExpenseChartLabels = [];
      this.conceptExpenseChartData = [];
      this.totalConceptExpense = 0;
      this.conceptsExpenseList.filter(concept =>concept.category === active[0]["_view"].label).forEach(element => {
        this.conceptExpenseChartLabels.push(element.concept);
        this.conceptExpenseChartData.push(element.value);
        this.totalConceptExpense += element.value;
      });
      for (let index = 0; index < 20; index++) {  //Introducir valores nulos para que genere los colores
        this.conceptExpenseChartData.push(null);
      }
      this.conceptExpenseChartTitle = active[0]["_view"].label;
    }
  }
  processBarChartData(data:any){
    let previousBalance: number = 0;
    this.balanceChartData[0].data = [];
    this.balanceChartData[1].data = [];
    this.balanceChartData[2].data = [];
    this.balanceChartLabels = [];
    let labeldates =[];
    data.forEach(element => {
      var date = new Date(element.date);
      this.balanceChartLabels.push(date.toDateString());
      labeldates.push(date);
      if(element.type == 'Ingreso'){
        this.balanceChartData[0].data.push(element.value);
        this.balanceChartData[1].data.push(null);
        this.balanceChartData[2].data.push(previousBalance + element.value);
        previousBalance += element.value;
      }else if(element.type == 'Gasto'){
        this.balanceChartData[0].data.push(null);
        this.balanceChartData[1].data.push( - element.value);
        this.balanceChartData[2].data.push(previousBalance - element.value);
        previousBalance -= element.value;
      }
    });
    this.balance = previousBalance;
    
    if(((labeldates[labeldates.length - 1] -labeldates[0])/86400000) < 28){
      let minDate = new Date(labeldates[0]);
      minDate.setMonth(minDate.getMonth()+1)
      this.balanceChartLabels.push((minDate).toDateString());
      this.balanceChartData[0].data.push(null);
      this.balanceChartData[1].data.push(null);
      this.balanceChartData[2].data.push(null);
    };
  }
  applyFilter(){
    let filter: AccountingFilter = this.accountingFilterService.appliedFilter;
    let deletingList: AccountingActivity[];
    let filteredList: AccountingActivity[] = this.activityListUncut;
    if (filter.type){
      let emptyType:boolean = true; 
      deletingList = filteredList;
      filter.type.forEach(type => {
        deletingList = deletingList.filter(element => element.type != type);
        emptyType = false;
      })
      if (emptyType){
        filter.type = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.category){     
      let emptyCategory:boolean = true; 
      deletingList = filteredList;
      filter.category.forEach(category => {
        deletingList = deletingList.filter(element => element.categoryKey != category);
        emptyCategory = false
      })
      if (emptyCategory){
        filter.category = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.concept){
      let emptyConcept:boolean = true;
      deletingList = filteredList;
      filter.concept.forEach(concept => {
        deletingList = deletingList.filter(element => element.conceptKey != concept);
        emptyConcept = false;
      })
      if (emptyConcept){
        filter.concept = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.house){
      let emptyHouse:boolean = true;
      deletingList = filteredList;
      filter.house.forEach(house => {
        deletingList = deletingList.filter(element => element.house != house);
        emptyHouse = false;
      })
      if (emptyHouse){
        filter.house = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.vehicle){
      let emptyVehicle:boolean = true;
      deletingList = filteredList;
      filter.vehicle.forEach(vehicle => {
        deletingList = deletingList.filter(element => element.vehicle != vehicle);
        emptyVehicle = false;
      })
      if (emptyVehicle){
        filter.vehicle = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.year){
      let emptyYear:boolean = true;
      deletingList = filteredList;
      filter.year.forEach(year => {
        deletingList = deletingList.filter(element =>{ 
          var date = new Date(element.date);
          var elementYear = date.getFullYear();
          return elementYear != +year
        });
        emptyYear = false;
      })
      if (emptyYear){
        filter.year = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }
    if (filter.month){
      let emptyMonth:boolean = true;
      deletingList = filteredList;
      filter.month.forEach(month => {
        deletingList = deletingList.filter(element =>{ 
          var date = new Date(element.date);
          var elementMonth = date.getMonth();
          return elementMonth != +month
        });
        emptyMonth = false;
      })
      if (emptyMonth){
        filter.month = null;
      }else{
        filteredList = filteredList.filter(x => !deletingList.includes(x));
      }
    }

    if (filter.startDate){
      deletingList = filteredList;
      if(filter.startDate["_d"]){
        filter.startDate = filter.startDate["_d"].toISOString();
      }
      deletingList = deletingList.filter(element => element.date < filter.startDate)
      filteredList = filteredList.filter(x => !deletingList.includes(x));
    }
    if (filter.endDate){
      deletingList = filteredList;
      if(filter.endDate["_d"]){
        filter.endDate = filter.endDate["_d"].toISOString();
      }
      deletingList = deletingList.filter(element => element.date > filter.endDate)
      filteredList = filteredList.filter(x => !deletingList.includes(x));
    }

    if (!filter.type && !filter.category && !filter.concept && !filter.year && !filter.month && !filter.startDate && !filter.house && !filter.vehicle){
      filteredList = this.activityListUncut;
    }
    this.activityList = filteredList;
    this.activityList.sort((a, b) => {return a.date.localeCompare(b.date)});
    this.processPieChartData(this.activityList);
    this.processBarChartData(this.activityList);
  }
}
