import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingHeaderComponent } from './accounting-header.component';

describe('AccountingHeaderComponent', () => {
  let component: AccountingHeaderComponent;
  let fixture: ComponentFixture<AccountingHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
