import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AccountingActivityFormComponent } from "../accounting-activity-form/accounting-activity-form.component";
import { AccountingFilterFormComponent } from "../accounting-filter-form/accounting-filter-form.component";
import { AccountingActivityConsultsService } from '../../../services/accounting/accounting-activity-consults.service';
import { AccountingCategoryConsultsService } from '../../../services/dropdowns/accounting-category-consults.service';
import { AccountingConceptConsultsService } from '../../../services/dropdowns/accounting-concept-consults.service';
import { HouseConsultsService } from '../../../services/dropdowns/house-consults.service';
import { VehicleConsultsService } from '../../../services/dropdowns/vehicle-consults.service';
import { AccountingFilterService } from '../../../services/accounting/accounting-filter.service';
import { AccountingActivity } from '../../../models/accounting/accounting-activity';
import { AccountingCategories } from '../../../models/dropdowns/accounting-categories';
import { AccountingConcepts } from '../../../models/dropdowns/accounting-concepts';
import { Houses } from '../../../models/dropdowns/houses';
import { Vehicles } from '../../../models/dropdowns/vehicles';
import { UserService } from '../../../services/users/user.service';
import { ActivatedRoute } from '@angular/router';
import { AccountingActivityTableComponent } from '../accounting-activity-table/accounting-activity-table.component';
import { AccountingAnnualSummaryComponent } from '../accounting-annual-summary/accounting-annual-summary.component';
import { AccountingMonthlySummaryComponent } from '../accounting-monthly-summary/accounting-monthly-summary.component';
import { AccountingGraphsComponent } from '../accounting-graphs/accounting-graphs.component';

@Component({
  selector: 'app-accounting-header',
  templateUrl: './accounting-header.component.html',
  styleUrls: ['./accounting-header.component.scss'],
  entryComponents:[ 
    AccountingActivityFormComponent,
    AccountingFilterFormComponent,
   ]
})
export class AccountingHeaderComponent implements OnInit {
  @ViewChild(AccountingActivityTableComponent) activityTableChild: AccountingActivityTableComponent;
  @ViewChild(AccountingAnnualSummaryComponent) annualTableChild: AccountingAnnualSummaryComponent;
  @ViewChild(AccountingMonthlySummaryComponent) monthlyTableChild: AccountingMonthlySummaryComponent;
  @ViewChild(AccountingGraphsComponent) activityGraphsChild: AccountingGraphsComponent;
  title:string = 'Contabilidad';
  userId: string;
  activityListUncut: AccountingActivity[];
  categoriesListUncut: AccountingCategories[];
  conceptsListUncut: AccountingConcepts[];
  housesListUncut: Houses[];
  vehiclesListUncut: Vehicles[];
  yearsList: number[] = [];
  selectedPage: string;
  previousSelectedPage: string;

  constructor(
    public dialog: MatDialog, 
    private activityConsultsService: AccountingActivityConsultsService,
    public accountingFilterService: AccountingFilterService,
    private categoryConsultsService: AccountingCategoryConsultsService,
    private conceptConsultsService: AccountingConceptConsultsService,
    private houseConsultsService: HouseConsultsService,
    private vehicleConsultsService: VehicleConsultsService,
    private userService: UserService,
    private route: ActivatedRoute,  
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.selectedPage = this.route.snapshot.data['selectedPage'];
        this.changeTitle(this.selectedPage);
        this.accountingFilterService.emptyApliedForm();
        this.activityConsultsService.initializeForm();
        this.getCategories();
        this.getConcepts();
        this.getHouses();
        this.getVehicles();
        this.getActivities();
      } 
    }); 
  }

  openInputForm(type:string): void {
    const dialogRef = this.dialog.open(AccountingActivityFormComponent, {
      width: '540px',
      panelClass: type,
      data: {
        type: type
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.activityConsultsService.initializeForm();
    });
  }
  
  openFiltersForm(): void {
    this.accountingFilterService.selectAppliedFilter();
    const dialogRef = this.dialog.open(AccountingFilterFormComponent, {
      width: '540px',
      //panelClass: type,
      data: {
        yearsList: this.yearsList,
        selectedPage: this.selectedPage,
        previousSelectedPage: this.previousSelectedPage
      }
    });
    this.previousSelectedPage = this.selectedPage
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    })
    dialogRef.afterClosed().subscribe(result => {
      //this.accountingFilterService.initializeForm();
      if(this.accountingFilterService.selectedToApplied){
        switch (this.selectedPage) {
          case 'accountingTable':
            this.activityTableChild.renderTable();
            break;
          case 'accountingAnnualSummary':
            this.annualTableChild.renderTable();
            break;
          case 'accountingMonthlySummary':
            this.monthlyTableChild.renderTable();
            break;
          case 'accountingGraphs':
            this.activityGraphsChild.applyFilter();
            break;        
          default:
            break;
        }

        //this.applyFilter();
      }
    });
  }


  getCategories(){
    var content = this.categoryConsultsService.getCategories(this.userId);
    var subscription = content.snapshotChanges().subscribe(category => {
      this.categoriesListUncut = [];
      category.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.categoriesListUncut.push(register as AccountingCategories)
      });
    });
  }

  getConcepts(){
    var content = this.conceptConsultsService.getConcepts(this.userId);
    var subscription = content.snapshotChanges().subscribe(concept => {
      this.conceptsListUncut = [];
      concept.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.conceptsListUncut.push(register as AccountingConcepts)
      });
    });
  }

  getHouses(){
    var content = this.houseConsultsService.getHouses(this.userId);
    var subscription = content.snapshotChanges().subscribe(concept => {
      this.housesListUncut = [];
      concept.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.housesListUncut.push(register as Houses)
      });
    });
  }

  getVehicles(){
    var content = this.vehicleConsultsService.getVehicles(this.userId);
    var subscription = content.snapshotChanges().subscribe(concept => {
      this.vehiclesListUncut = [];
      concept.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.vehiclesListUncut.push(register as Vehicles)
      });
    });
  }

  getActivities(){
    var content = this.activityConsultsService.getActivities(this.userId);
    var subscription = content.snapshotChanges().subscribe(activity => {
      this.activityListUncut = [];
      activity.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        register["category"] = this.categoriesListUncut.find( category => category.$key === register['categoryKey'] ).category;
        register["concept"] = this.conceptsListUncut.find( concept => concept.$key === register['conceptKey'] ).concept; 
        this.activityListUncut.push(register as AccountingActivity)

        var date = new Date(register["date"]);
        var elementYear = date.getFullYear()
        if (this.yearsList.indexOf(elementYear) === -1) {this.yearsList.push(elementYear)};
      })
      this.activityListUncut.sort((a, b) => {return b.date.localeCompare(a.date)})
      this.yearsList.sort();
      this.yearsList.reverse();
    });  
  }
  changeTitle(page: string){
    switch (page) {
      case 'accountingGraphs':
        if (window.innerWidth <= 570){
          this.title = 'Cont. - Graficos '
        }else{
          this.title = 'Contabilidad - Graficos '
        }
        break;
      case 'accountingTable':
        if (window.innerWidth <= 570){
          this.title = 'Cont. - Tabla' 
        }else{
          this.title = 'Contabilidad - Tabla'
        }
        break;
      case 'accountingMonthlySummary':
        if (window.innerWidth <= 570){
          this.title = 'Cont. - Mes'
        }else{
          this.title = 'Contabilidad - Mensual'
        }
        break;
      case 'accountingAnnualSummary':
        if (window.innerWidth <= 570){
          this.title = 'Cont. - Año' 
        }else{
          this.title = 'Contabilidad - Anual' 
        }
        break;
        
      default:
        break;
    }
  }
}