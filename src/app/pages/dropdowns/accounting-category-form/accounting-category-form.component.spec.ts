import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingCategoryFormComponent } from './accounting-category-form.component';

describe('AccountingCategoryFormComponent', () => {
  let component: AccountingCategoryFormComponent;
  let fixture: ComponentFixture<AccountingCategoryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingCategoryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
