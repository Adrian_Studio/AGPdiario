import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { AccountingCategoryConsultsService } from '../../../services/dropdowns/accounting-category-consults.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/users/user.service';
import { NgForm } from '@angular/forms';;

@Component({
  selector: 'app-accounting-category-form',
  templateUrl: './accounting-category-form.component.html',
  styleUrls: ['./accounting-category-form.component.scss']
})
export class AccountingCategoryFormComponent implements OnInit {
  userId: string;
  keyBackUp: string;
  userIdBackup: string;

  constructor(public dialogRef: MatDialogRef<AccountingCategoryConsultsService>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public categoryConsultsService: AccountingCategoryConsultsService,
    private toastrService: ToastrService,
    private userService: UserService,
    ) { 
      this.categoryConsultsService.selectedCategory.type = this.data['type'];
    }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
      } 
    });
  }


  onSubmit(categoryForm: NgForm){
    if(this.categoryConsultsService.selectedCategory.$key == null){
      this.categoryConsultsService.addCategory(categoryForm.value);
      this.toastrService.success('Operación ejecutada con exito','Añadir Categoría')
      this.dialogRef.close();
    }else{
      this.categoryConsultsService.updateCategory(categoryForm.value);
      this.toastrService.success('Operación ejecutada con exito','Editar Categoría')
      this.dialogRef.close();
    }
  }

  resetForm(categoryForm: NgForm){
    this.keyBackUp = categoryForm.value.$key;
    this.userIdBackup = categoryForm.value.userId;
    if (categoryForm != null){
      categoryForm.reset();
    }
    categoryForm.value.$key = this.keyBackUp;
    categoryForm.value.userId = this.userIdBackup;
  }
}

