import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingCategoryTableComponent } from './accounting-category-table.component';

describe('AccountingCategoryTableComponent', () => {
  let component: AccountingCategoryTableComponent;
  let fixture: ComponentFixture<AccountingCategoryTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingCategoryTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingCategoryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
