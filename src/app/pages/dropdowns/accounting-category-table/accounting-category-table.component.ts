import { Component, OnInit, ViewChild } from '@angular/core';
import { AccountingCategoryConsultsService } from '../../../services/dropdowns/accounting-category-consults.service';
import { AccountingActivityConsultsService } from '../../../services/accounting/accounting-activity-consults.service';
import { AccountingConceptConsultsService } from '../../../services/dropdowns/accounting-concept-consults.service';
import { AccountingCategories } from '../../../models/dropdowns/accounting-categories';
import { ToastrService } from 'ngx-toastr';
import { AccountingCategoryFormComponent } from "../accounting-category-form/accounting-category-form.component";
import { MatDialog } from '@angular/material';
import { UserService } from '../../../services/users/user.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-accounting-category-table',
  templateUrl: './accounting-category-table.component.html',
  styleUrls: ['./accounting-category-table.component.scss'],
  entryComponents:[ AccountingCategoryFormComponent ]
})
export class AccountingCategoryTableComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = { };
  categoriesList: AccountingCategories[];
  userId: string;
  dtTrigger: Subject<any> = new Subject();
  isRendered: boolean = false;
  refreshNeeded: boolean = true;
  lengthDB: number;

  constructor(
      private categoryConsultsService: AccountingCategoryConsultsService,
      private activityConsultsService: AccountingActivityConsultsService,
      private conceptConsultsService: AccountingConceptConsultsService,
      private toastrService: ToastrService,
      private userService: UserService,
      public dialog: MatDialog,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.categoryConsultsService.initializeForm();
        this.datatableOptions();
        this.getCategories();
      } 
    }); 
  }
  
  getCategories(){
    var content = this.categoryConsultsService.getCategories(this.userId);
    content.snapshotChanges().subscribe(categories => {
      if(this.refreshNeeded || categories.length != this.lengthDB){
        $('.loading').removeClass('hidden');
        $('.table-responsive table').addClass('hidden');
        if(this.isRendered){
          this.destroyTable();
        }else{
          this.categoriesList = [];
          categories.forEach( element => {
            var register = element.payload.toJSON();
            register["$key"] = element.key;
            this.categoriesList.push(register as AccountingCategories)
          })
          this.refreshNeeded = false;  
          this.dtTrigger.next();
          this.isRendered = true;     
          this.lengthDB = categories.length;
        }
      }
    });  
  }

  editCategory(category){
    this.refreshNeeded = true;
    const dialogRef = this.dialog.open(AccountingCategoryFormComponent, {
      width: '300px',
      panelClass: category.type,
      data: {
        type: category.type,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.categoryConsultsService.initializeForm();
    });
    this.categoryConsultsService.selectedCategory = Object.assign({},category);;   
  }

  deleteCategory(category){
    this.refreshNeeded = true;
    if(confirm("¿Esta seguro de que quiere eliminar el registro?. Se eliminaran todos los registros con esta localizacion de la base de datos de inventario") == true){
      this.categoryConsultsService.deleteCategory(category);
      this.deleteActivityCategory(category);
      this.deleteConceptCategory(category);
      this.toastrService.success('Operación ejecutada con exito','Eliminar localizacion');
    }
  }
  deleteActivityCategory(category){
    var content = this.activityConsultsService.getActivities(this.userId);
    var subscription = content.snapshotChanges().subscribe(categories => {
      categories.forEach( element => {
        var register = element.payload.toJSON();
        if(register["category"] == category){
          this.activityConsultsService.deleteActivity(element.key);
        }
      })
    });
  }
  deleteConceptCategory(category){
    var content = this.conceptConsultsService.getConcepts(this.userId);
    var subscription = content.snapshotChanges().subscribe(concepts => {
      concepts.forEach( element => {
        var register = element.payload.toJSON();
        if(register["category"] == category){
          this.conceptConsultsService.deleteConcept(element.key);
        }
      })
    });
  }
  datatableOptions(){
    this.dtOptions = {
      responsive: true,
      scrollY: (window.innerHeight - 20),
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      order: [[ 0, "desc" ], [ 1, "asc" ]],
      columnDefs: [
        { responsivePriority: 0, targets: 1 },
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: 2 },
      ],
      initComplete: function(settings, json) {
        $('.loading').addClass('hidden');
        $('.table-responsive table').removeClass('hidden');
        var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
        $('div.dataTables_scrollBody').height( scrollY ); 
      }
    };
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  destroyTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.isRendered = false;
      this.getCategories()
    });
  }
}
