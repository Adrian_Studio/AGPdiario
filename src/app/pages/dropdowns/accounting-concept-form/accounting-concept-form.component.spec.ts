import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingConceptFormComponent } from './accounting-concept-form.component';

describe('AccountingConceptFormComponent', () => {
  let component: AccountingConceptFormComponent;
  let fixture: ComponentFixture<AccountingConceptFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingConceptFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingConceptFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
