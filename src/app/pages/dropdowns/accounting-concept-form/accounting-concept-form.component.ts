import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { AccountingConceptConsultsService } from '../../../services/dropdowns/accounting-concept-consults.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/users/user.service';
import { NgForm } from '@angular/forms';
import { AccountingCategoryConsultsService } from '../../../services/dropdowns/accounting-category-consults.service';
import { AccountingCategories } from 'src/app/models/dropdowns/accounting-categories';


@Component({
  selector: 'app-accounting-concept-form',
  templateUrl: './accounting-concept-form.component.html',
  styleUrls: ['./accounting-concept-form.component.scss']
})
export class AccountingConceptFormComponent implements OnInit {
  userId: string;
  keyBackUp: string;
  userIdBackup: string;
  categoriesList: AccountingCategories[];
  constructor(public dialogRef: MatDialogRef<AccountingConceptConsultsService>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public conceptConsultsService: AccountingConceptConsultsService,
    public categoryConsultsService: AccountingCategoryConsultsService,
    private toastrService: ToastrService,
    private userService: UserService,
    ) { 
      this.conceptConsultsService.selectedConcept.type = this.data['type'];
    }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.getCategories();
      } 
    });
  }


  onSubmit(conceptForm: NgForm){
    if(this.conceptConsultsService.selectedConcept.$key == null){
      this.conceptConsultsService.addConcept(conceptForm.value);
      this.toastrService.success('Operación ejecutada con exito','Añadir Concepto')
      this.dialogRef.close();
    }else{
      this.conceptConsultsService.updateConcept(conceptForm.value);
      this.toastrService.success('Operación ejecutada con exito','Editar Concepto')
      this.dialogRef.close();
    }
  }

  resetForm(conceptForm: NgForm){
    this.keyBackUp = conceptForm.value.$key;
    this.userIdBackup = conceptForm.value.userId;
    if (conceptForm != null){
      conceptForm.reset();
    }
    conceptForm.value.$key = this.keyBackUp;
    conceptForm.value.userId = this.userIdBackup;
  }

  getCategories(){
    var content = this.categoryConsultsService.getCategories(this.userId);
    content.snapshotChanges().subscribe(category => {
      this.categoriesList = [];
      category.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        if(register["type"] == this.data['type']){
          this.categoriesList.push(register as AccountingCategories)
        }
      });
    });
  }
}

