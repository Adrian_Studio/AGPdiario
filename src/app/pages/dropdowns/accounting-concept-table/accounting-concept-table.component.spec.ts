import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingConceptTableComponent } from './accounting-concept-table.component';

describe('AccountingConceptTableComponent', () => {
  let component: AccountingConceptTableComponent;
  let fixture: ComponentFixture<AccountingConceptTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingConceptTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingConceptTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
