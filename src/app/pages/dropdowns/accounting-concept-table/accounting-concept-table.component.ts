import { Component, OnInit, ViewChild } from '@angular/core';
import { AccountingConceptConsultsService } from '../../../services/dropdowns/accounting-concept-consults.service';
import { AccountingActivityConsultsService } from '../../../services/accounting/accounting-activity-consults.service';
import { AccountingConcepts } from '../../../models/dropdowns/accounting-concepts';
import { ToastrService } from 'ngx-toastr';
import { AccountingConceptFormComponent } from "../accounting-concept-form/accounting-concept-form.component";
import { MatDialog } from '@angular/material';
import { UserService } from '../../../services/users/user.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { AccountingCategories } from 'src/app/models/dropdowns/accounting-categories';
import { AccountingCategoryConsultsService } from '../../../services/dropdowns/accounting-category-consults.service';

@Component({
  selector: 'app-accounting-concept-table',
  templateUrl: './accounting-concept-table.component.html',
  styleUrls: ['./accounting-concept-table.component.scss'],
  entryComponents:[ AccountingConceptFormComponent ]
})
export class AccountingConceptTableComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = { };
  conceptsList: AccountingConcepts[];
  userId: string;
  dtTrigger: Subject<any> = new Subject();
  isRendered: boolean = false;
  refreshNeeded: boolean = true;
  lengthDB: number;
  categoryList: AccountingCategories[];

  constructor(
      private conceptConsultsService: AccountingConceptConsultsService,
      private activityConsultsService: AccountingActivityConsultsService,
      private categoryConsultsService: AccountingCategoryConsultsService,
      private toastrService: ToastrService,
      private userService: UserService,
      public dialog: MatDialog,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.conceptConsultsService.initializeForm();
        this.datatableOptions();
        this.getCategories();
        this.getConcepts();
      } 
    }); 
  }
  
  getConcepts(){
    var content = this.conceptConsultsService.getConcepts(this.userId);
    content.snapshotChanges().subscribe(concepts => {
      if(this.refreshNeeded || concepts.length != this.lengthDB){
        $('.loading').removeClass('hidden');
        $('.table-responsive table').addClass('hidden');
        if(this.isRendered){
          this.destroyTable();
        }else{
          this.conceptsList = [];
          concepts.forEach( element => {
            var register = element.payload.toJSON();
            register["$key"] = element.key;
            register["category"] = this.categoryList.find( category => category.$key === register['category'] ).category;
            this.conceptsList.push(register as AccountingConcepts)
          })
          this.refreshNeeded = false;  
          this.dtTrigger.next();
          this.isRendered = true;     
          this.lengthDB = concepts.length;
        }
      }
    });  
  }

  editConcept(concept){
    this.refreshNeeded = true;
    const dialogRef = this.dialog.open(AccountingConceptFormComponent, {
      width: '300px',
      panelClass: concept.type,
      data: {
        type: concept.type,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.conceptConsultsService.initializeForm();
    });
    this.conceptConsultsService.selectedConcept = Object.assign({},concept);
    this.conceptConsultsService.selectedConcept.type = this.categoryList.find( category => category.category === this.conceptConsultsService.selectedConcept.category ).type;
    this.conceptConsultsService.selectedConcept.category = this.categoryList.find( category => category.category === this.conceptConsultsService.selectedConcept.category ).$key;
  }

  deleteConcept(concept){
    this.refreshNeeded = true;
    if(confirm("¿Esta seguro de que quiere eliminar el registro?. Se eliminaran todos los registros con esta localizacion de la base de datos de inventario") == true){
      this.conceptConsultsService.deleteConcept(concept);
      this.deleteActivityConcept(concept);
      this.toastrService.success('Operación ejecutada con exito','Eliminar localizacion');
    }
  }
  deleteActivityConcept(concept){
    var content = this.activityConsultsService.getActivities(this.userId);
    content.snapshotChanges().subscribe(activity => {
      activity.forEach( element => {
        var register = element.payload.toJSON();
        if(register["concept"] == concept){
          this.activityConsultsService.deleteActivity(element.key);
        }
      })
    });
  }
  datatableOptions(){
    this.dtOptions = {
      responsive: true,
      scrollY: (window.innerHeight - 20),
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      order: [[ 0, "asc" ], [ 1, "asc" ]],
      columnDefs: [
        { responsivePriority: 0, targets: 0 },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: 2 },
      ],
      initComplete: function(settings, json) {
        $('.loading').addClass('hidden');
        $('.table-responsive table').removeClass('hidden');
        var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
        $('div.dataTables_scrollBody').height( scrollY ); 
      }
    };
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  destroyTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.isRendered = false;
      this.getConcepts()
    });
  }
  getCategories(){
    var content = this.categoryConsultsService.getCategories(this.userId);
    var subscription = content.snapshotChanges().subscribe(category => {
      this.categoryList = [];
      category.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.categoryList.push(register as AccountingCategories)
      });
    });
  }
}
