import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownsHeaderComponent } from './dropdowns-header.component';

describe('DropdownsHeaderComponent', () => {
  let component: DropdownsHeaderComponent;
  let fixture: ComponentFixture<DropdownsHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownsHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
