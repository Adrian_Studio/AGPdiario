import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AccountingCategoryFormComponent } from "../accounting-category-form/accounting-category-form.component";
import { AccountingConceptFormComponent } from "../accounting-concept-form/accounting-concept-form.component";
import { HouseFormComponent } from "../house-form/house-form.component";
import { VehicleFormComponent } from "../vehicle-form/vehicle-form.component";
import { InventoryElementTypeFormComponent } from "../inventory-element-type-form/inventory-element-type-form.component";
import { InventoryLocationFormComponent } from "../inventory-location-form/inventory-location-form.component";
import { AccountingCategoryConsultsService } from '../../../services/dropdowns/accounting-category-consults.service';
import { AccountingConceptConsultsService } from '../../../services/dropdowns/accounting-concept-consults.service';
import { HouseConsultsService } from '../../../services/dropdowns/house-consults.service';
import { VehicleConsultsService } from '../../../services/dropdowns/vehicle-consults.service';
import { InventoryElementTypeConsultsService } from '../../../services/dropdowns/inventory-element-type-consults.service';
import { InventoryLocationConsultsService } from '../../../services/dropdowns/inventory-location-consults.service';
import { AccountingActivity } from '../../../models/accounting/accounting-activity';
import { AccountingCategories } from '../../../models/dropdowns/accounting-categories';
import { AccountingConcepts } from '../../../models/dropdowns/accounting-concepts';
import { AccountingFilter } from '../../../models/accounting/accounting-filter';
import { Houses } from '../../../models/dropdowns/houses';
import { Vehicles } from '../../../models/dropdowns/vehicles';
import { UserService } from '../../../services/users/user.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-dropdowns-header',
  templateUrl: './dropdowns-header.component.html',
  styleUrls: ['./dropdowns-header.component.scss'],
  entryComponents:[ 
    AccountingCategoryFormComponent,
    AccountingConceptFormComponent,
    HouseFormComponent,
    VehicleFormComponent,
    InventoryElementTypeFormComponent,
    InventoryLocationFormComponent,
   ]
})
export class DropdownsHeaderComponent implements OnInit {
  title:string = 'asdf';
  userId: string;
  selectedPage: string;

  constructor(
    public dialog: MatDialog, 
    private categoryConsultsService: AccountingCategoryConsultsService,
    private conceptConsultsService: AccountingConceptConsultsService,
    private houseConsultsService: HouseConsultsService,
    private vehicleConsultsService: VehicleConsultsService,
    private inventoryElementTypeConsultsService: InventoryElementTypeConsultsService,
    private inventoryLocationConsultsService: InventoryLocationConsultsService,
    private userService: UserService,
    private route: ActivatedRoute,  
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.selectedPage = this.route.snapshot.data['selectedPage'];
        this.changeTitle(this.selectedPage);
      } 
    }); 
  }

  openInputForm(type:string): void {
    let dialogRef;
    switch (this.selectedPage) {
      case 'accountingCategories':
        dialogRef = this.dialog.open(AccountingCategoryFormComponent, {
          width: '300px',
          panelClass: type,
          data: {
            type: type
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          this.categoryConsultsService.initializeForm();
        });
        break;
      case 'accountingConcepts':
        dialogRef = this.dialog.open(AccountingConceptFormComponent, {
          width: '300px',
          panelClass: type,
          data: {
            type: type
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          this.conceptConsultsService.initializeForm();
        });
        break;
        case 'houses':
        dialogRef = this.dialog.open(HouseFormComponent, {
          width: '300px',
          panelClass: type,
          data: {
            type: type
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          this.houseConsultsService.initializeForm();
        });
        break;
        case 'vehicles':
        dialogRef = this.dialog.open(VehicleFormComponent, {
          width: '300px',
          panelClass: type,
          data: {
            type: type
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          this.vehicleConsultsService.initializeForm();
        });
        break;
      case 'inventoryType':
        dialogRef = this.dialog.open(InventoryElementTypeFormComponent, {
          width: '300px',
          panelClass: type,
          data: {
            type: type
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          this.inventoryElementTypeConsultsService.initializeForm();
        });
        break;
      case 'inventoryLocation':
        dialogRef = this.dialog.open(InventoryLocationFormComponent, {
          width: '300px',
          panelClass: type,
          data: {
            type: type
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          this.inventoryLocationConsultsService.initializeForm();
        });
        break;
    
      default:
        break;
    }
  }
  changeTitle(page: string){
    switch (page) {
      case 'accountingCategories':
        if (window.innerWidth <= 570){
          this.title = 'D. Categ.'
        }else{
          this.title = 'Desp. Contabilidad - Categorías'
        }
        break;
      case 'accountingConcepts':
        if (window.innerWidth <= 570){
          this.title = 'D. Concep.' 
        }else{
          this.title = 'Desp. Contabilidad - Conceptos'
        }
        break;
      case 'houses':
        if (window.innerWidth <= 570){
          this.title = 'D. Viviendas'
        }else{
          this.title = 'Desplegable Viviendas'
        }
        break;
      case 'vehicles':
        if (window.innerWidth <= 570){
          this.title = 'D. Vehículos' 
        }else{
          this.title = 'Desplegable Vehículos' 
        }
        break;
      case 'inventoryType':
        if (window.innerWidth <= 570){
          this.title = 'D. - Tipos elemento'
        }else{
          this.title = 'Desp. Inventario - Tipos elemento'
        }
        break;
      case 'inventoryLocation':
        if (window.innerWidth <= 570){
          this.title = 'D. - Localizaciones' 
        }else{
          this.title = 'Desp. Inventario - Localizaciones Inventario' 
        }
        break;
              
      default:
        break;
    }
  }
}