import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { HouseConsultsService } from '../../../services/dropdowns/house-consults.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/users/user.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-house-form',
  templateUrl: './house-form.component.html',
  styleUrls: ['./house-form.component.scss']
})
export class HouseFormComponent implements OnInit {
  userId: string;
  keyBackUp: string;
  userIdBackup: string;
  constructor(public dialogRef: MatDialogRef<HouseConsultsService>,
    public houseConsultsService: HouseConsultsService,
    private toastrService: ToastrService,
    private userService: UserService,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
      } 
    });
  }


  onSubmit(houseForm: NgForm){
    if(this.houseConsultsService.selectedHouse.$key == null){
      this.houseConsultsService.addHouse(houseForm.value);
      this.toastrService.success('Operación ejecutada con exito','Añadir Vivienda')
      this.dialogRef.close();
    }else{
      this.houseConsultsService.updateHouse(houseForm.value);
      this.toastrService.success('Operación ejecutada con exito','Editar Vivienda')
      this.dialogRef.close();
    }
  }

  resetForm(houseForm: NgForm){
    this.keyBackUp = houseForm.value.$key;
    this.userIdBackup = houseForm.value.userId;
    if (houseForm != null){
      houseForm.reset();
    }
    houseForm.value.$key = this.keyBackUp;
    houseForm.value.userId = this.userIdBackup;
  }
}

