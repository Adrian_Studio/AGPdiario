import { Component, OnInit, ViewChild } from '@angular/core';
import { HouseConsultsService } from '../../../services/dropdowns/house-consults.service';
import { AccountingActivityConsultsService } from '../../../services/accounting/accounting-activity-consults.service';
import { Houses } from '../../../models/dropdowns/houses';
import { ToastrService } from 'ngx-toastr';
import { HouseFormComponent } from "../house-form/house-form.component";
import { MatDialog } from '@angular/material';
import { UserService } from '../../../services/users/user.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-house-table',
  templateUrl: './house-table.component.html',
  styleUrls: ['./house-table.component.scss'],
  entryComponents:[ HouseFormComponent ]
})
export class HouseTableComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = { };
  housesList: Houses[];
  userId: string;
  dtTrigger: Subject<any> = new Subject();
  isRendered: boolean = false;
  refreshNeeded: boolean = true;
  lengthDB: number;

  constructor(
      private houseConsultsService: HouseConsultsService,
      private activityConsultsService: AccountingActivityConsultsService,
      private toastrService: ToastrService,
      private userService: UserService,
      public dialog: MatDialog,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.houseConsultsService.initializeForm();
        this.datatableOptions();
        this.getHouses();
      } 
    }); 
  }
  
  getHouses(){
    var content = this.houseConsultsService.getHouses(this.userId);
    content.snapshotChanges().subscribe(houses => {
      if(this.refreshNeeded || houses.length != this.lengthDB){
        $('.loading').removeClass('hidden');
        $('.table-responsive table').addClass('hidden');
        if(this.isRendered){
          this.destroyTable();
        }else{
          this.housesList = [];
          houses.forEach( element => {
            var register = element.payload.toJSON();
            register["$key"] = element.key;
            this.housesList.push(register as Houses)
          })
          this.refreshNeeded = false;  
          this.dtTrigger.next();
          this.isRendered = true;     
          this.lengthDB = houses.length;
        }
      }
    });  
  }

  editHouse(house){
    this.refreshNeeded = true;
    const dialogRef = this.dialog.open(HouseFormComponent, {
      width: '300px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.houseConsultsService.initializeForm();
    });
    this.houseConsultsService.selectedHouse = Object.assign({},house);;   
  }

  deleteHouse(house){
    this.refreshNeeded = true;
    if(confirm("¿Esta seguro de que quiere eliminar el registro?. Se eliminaran todos los registros con esta localizacion de la base de datos de inventario") == true){
      this.houseConsultsService.deleteHouse(house);
      this.deleteActivityHouse(house);
      this.toastrService.success('Operación ejecutada con exito','Eliminar localizacion');
    }
  }
  deleteActivityHouse(house){
    var content = this.activityConsultsService.getActivities(this.userId);
    var subscription = content.snapshotChanges().subscribe(houses => {
      houses.forEach( element => {
        var register = element.payload.toJSON();
        if(register["house"] == house){
          this.activityConsultsService.deleteActivity(element.key);
        }
      })
    });
  }
  datatableOptions(){
    this.dtOptions = {
      responsive: true,
      scrollY: (window.innerHeight - 20),
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      order: [[ 0, "desc" ], [ 1, "asc" ]],
      columnDefs: [
        { responsivePriority: 0, targets: 1 },
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: 2 },
      ],
      initComplete: function(settings, json) {
        $('.loading').addClass('hidden');
        $('.table-responsive table').removeClass('hidden');
        var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
        $('div.dataTables_scrollBody').height( scrollY ); 
      }
    };
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  destroyTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.isRendered = false;
      this.getHouses()
    });
  }
}
