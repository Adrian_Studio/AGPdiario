import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryElementTypeFormComponent } from './inventory-element-type-form.component';

describe('InventoryElementTypeFormComponent', () => {
  let component: InventoryElementTypeFormComponent;
  let fixture: ComponentFixture<InventoryElementTypeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryElementTypeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryElementTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
