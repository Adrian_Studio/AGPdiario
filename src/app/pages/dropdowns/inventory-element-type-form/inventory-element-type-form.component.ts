import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { InventoryElementTypeConsultsService } from '../../../services/dropdowns/inventory-element-type-consults.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/users/user.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-inventory-element-type-form',
  templateUrl: './inventory-element-type-form.component.html',
  styleUrls: ['./inventory-element-type-form.component.scss']
})
export class InventoryElementTypeFormComponent implements OnInit {
  userId: string;
  keyBackUp: string;
  userIdBackup: string;
  constructor(public dialogRef: MatDialogRef<InventoryElementTypeFormComponent>,
    public elementTypeConsultsService: InventoryElementTypeConsultsService,
    private toastrService: ToastrService,
    private userService: UserService,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
      } 
    });
    
  }


  onSubmit(elementTypeForm: NgForm){
    if(this.elementTypeConsultsService.selectedElementType.$key == null){
      this.elementTypeConsultsService.addElementType(elementTypeForm.value);
      this.toastrService.success('Operación ejecutada con exito','Añadir Tipo de elemento')
      this.dialogRef.close();
    }else{
      this.elementTypeConsultsService.updateElementType(elementTypeForm.value);
      this.toastrService.success('Operación ejecutada con exito','Editar Tipo de elemento')
      this.dialogRef.close();
    }
  }

  resetForm(elementTypeForm: NgForm){
    this.keyBackUp = elementTypeForm.value.$key;
    this.userIdBackup = elementTypeForm.value.userId;
    if (elementTypeForm != null){
      elementTypeForm.reset();
    }
    elementTypeForm.value.$key = this.keyBackUp;
    elementTypeForm.value.userId = this.userIdBackup;
  }

}
