import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryElementTypeTableComponent } from './inventory-element-type-table.component';

describe('InventoryElementTypeTableComponent', () => {
  let component: InventoryElementTypeTableComponent;
  let fixture: ComponentFixture<InventoryElementTypeTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryElementTypeTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryElementTypeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
