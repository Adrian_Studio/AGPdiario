import { Component, OnInit, ViewChild } from '@angular/core';
import { InventoryElementTypeConsultsService } from '../../../services/dropdowns/inventory-element-type-consults.service';
import { InventoryItemsConsultsService } from '../../../services/inventory/inventory-items-consults.service';
import { InventoryElementType } from '../../../models/dropdowns/inventory-element-type';
import { ToastrService } from 'ngx-toastr';
import { InventoryElementTypeFormComponent } from "../inventory-element-type-form/inventory-element-type-form.component";
import { MatDialog } from '@angular/material';
import { UserService } from '../../../services/users/user.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-inventory-element-type-table',
  templateUrl: './inventory-element-type-table.component.html',
  styleUrls: ['./inventory-element-type-table.component.scss'],
  entryComponents:[ InventoryElementTypeFormComponent ]
})
export class InventoryElementTypeTableComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = { };
  inventoryElementTypeList: InventoryElementType[];
  userId: string;
  dtTrigger: Subject<any> = new Subject();
  isRendered: boolean = false;
  refreshNeeded: boolean = true;
  lengthDB: number;

  constructor(
      private elementTypeConsultsService: InventoryElementTypeConsultsService,
      private itemsConsultsService: InventoryItemsConsultsService,
      private toastrService: ToastrService,
      private userService: UserService,
      public dialog: MatDialog,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.elementTypeConsultsService.initializeForm();
        this.datatableOptions();
        this.getElementTypes();
      } 
    }); 
  }
  
  getElementTypes(){
    var content = this.elementTypeConsultsService.getElementTypes(this.userId);
    content.snapshotChanges().subscribe(inventoryElementType => {
      if(this.refreshNeeded || inventoryElementType.length != this.lengthDB){
        $('.loading').removeClass('hidden');
        $('.table-responsive table').addClass('hidden');
        if(this.isRendered){
          this.destroyTable();
        }else{
          this.inventoryElementTypeList = [];
          inventoryElementType.forEach( element => {
            var register = element.payload.toJSON();
            register["$key"] = element.key;
            this.inventoryElementTypeList.push(register as InventoryElementType)
          })
          this.refreshNeeded = false;  
          this.dtTrigger.next();
          this.isRendered = true;     
          this.lengthDB = inventoryElementType.length;
        }
      }
    });  
  }

  editElementType(elementType){
    this.refreshNeeded = true;
    const dialogRef = this.dialog.open(InventoryElementTypeFormComponent, {
      width: '540px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.elementTypeConsultsService.initializeForm();
    });
    this.elementTypeConsultsService.selectedElementType = Object.assign({},elementType);;   
  }

  deleteElementType(elementType){
    this.refreshNeeded = true;
    if(confirm("¿Esta seguro de que quiere eliminar el registro?. Se eliminaran todos los registros con este tipo de elemento de la base de datos de inventario.") == true){
      this.elementTypeConsultsService.deleteElementType(elementType);
      this.deleteItemsElementType(elementType);
      this.toastrService.success('Operación ejecutada con exito','Eliminar localizacion');
    }
  }
  deleteItemsElementType(elementType){
    var content = this.itemsConsultsService.getItems(this.userId);
    var subscription = content.snapshotChanges().subscribe(inventoryElementType => {
      inventoryElementType.forEach( element => {
        var register = element.payload.toJSON();
        if(register["elementType"] == elementType){
          this.itemsConsultsService.deleteItem(element.key);
        }
      })
    });
  }
  datatableOptions(){
    this.dtOptions = {
      responsive: true,
      scrollY: (window.innerHeight - 20),
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      order: [[ 0, "desc" ], [ 1, "asc" ]],
      columnDefs: [
        { responsivePriority: 0, targets: 1 },
        { responsivePriority: 1, targets: 0 },
      ],
      initComplete: function(settings, json) {
        $('.loading').addClass('hidden');
        $('.table-responsive table').removeClass('hidden');
        var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
        $('div.dataTables_scrollBody').height( scrollY ); 
      }
    };
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  destroyTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.isRendered = false;
      this.getElementTypes()
    });
  }

}
