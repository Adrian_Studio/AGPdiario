import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryLocationFormComponent } from './inventory-location-form.component';

describe('InventoryLocationFormComponent', () => {
  let component: InventoryLocationFormComponent;
  let fixture: ComponentFixture<InventoryLocationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryLocationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryLocationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
