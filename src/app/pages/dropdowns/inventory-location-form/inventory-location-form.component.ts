import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { InventoryLocationConsultsService } from '../../../services/dropdowns/inventory-location-consults.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/users/user.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-inventory-location-form',
  templateUrl: './inventory-location-form.component.html',
  styleUrls: ['./inventory-location-form.component.scss']
})
export class InventoryLocationFormComponent implements OnInit {
  userId: string;
  keyBackUp: string;
  userIdBackup: string;
  constructor(public dialogRef: MatDialogRef<InventoryLocationFormComponent>,
    public locationConsultsService: InventoryLocationConsultsService,
    private toastrService: ToastrService,
    private userService: UserService,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
      } 
    });
    
  }


  onSubmit(locationForm: NgForm){
    if(this.locationConsultsService.selectedLocation.$key == null){
      this.locationConsultsService.addLocation(locationForm.value);
      this.toastrService.success('Operación ejecutada con exito','Añadir Localización')
      this.dialogRef.close();
    }else{
      this.locationConsultsService.updateLocation(locationForm.value);
      this.toastrService.success('Operación ejecutada con exito','Editar Localización')
      this.dialogRef.close();
    }
  }

  resetForm(locationForm: NgForm){
    this.keyBackUp = locationForm.value.$key;
    this.userIdBackup = locationForm.value.userId;
    if (locationForm != null){
      locationForm.reset();
    }
    locationForm.value.$key = this.keyBackUp;
    locationForm.value.userId = this.userIdBackup;
  }
}
