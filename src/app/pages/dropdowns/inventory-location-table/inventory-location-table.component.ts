import { Component, OnInit, ViewChild } from '@angular/core';
import { InventoryLocationConsultsService } from '../../../services/dropdowns/inventory-location-consults.service';
import { InventoryItemsConsultsService } from '../../../services/inventory/inventory-items-consults.service';
import { InventoryLocation } from '../../../models/dropdowns/inventory-location';
import { ToastrService } from 'ngx-toastr';
import { InventoryLocationFormComponent } from "../inventory-location-form/inventory-location-form.component";
import { MatDialog } from '@angular/material';
import { UserService } from '../../../services/users/user.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-inventory-location-table',
  templateUrl: './inventory-location-table.component.html',
  styleUrls: ['./inventory-location-table.component.scss'],
  entryComponents:[ InventoryLocationFormComponent ]
})
export class InventoryLocationTableComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = { };
  inventoryLocationList: InventoryLocation[];
  userId: string;
  dtTrigger: Subject<any> = new Subject();
  isRendered: boolean = false;
  refreshNeeded: boolean = true;
  lengthDB: number;

  constructor(
      private locationConsultsService: InventoryLocationConsultsService,
      private itemsConsultsService: InventoryItemsConsultsService,
      private toastrService: ToastrService,
      private userService: UserService,
      public dialog: MatDialog,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.locationConsultsService.initializeForm();
        this.datatableOptions();
        this.getLocations();
      } 
    }); 
  }
  
  getLocations(){
    var content = this.locationConsultsService.getLocations(this.userId);
    content.snapshotChanges().subscribe(inventoryLocation => {
      if(this.refreshNeeded || inventoryLocation.length != this.lengthDB){
        $('.loading').removeClass('hidden');
        $('.table-responsive table').addClass('hidden');
        if(this.isRendered){
          this.destroyTable();
        }else{
          this.inventoryLocationList = [];
          inventoryLocation.forEach( element => {
            var register = element.payload.toJSON();
            register["$key"] = element.key;
            this.inventoryLocationList.push(register as InventoryLocation)
          })
          this.refreshNeeded = false;  
          this.dtTrigger.next();
          this.isRendered = true;     
          this.lengthDB = inventoryLocation.length;
        }
      }
    });  
  }

  editLocation(location){
    this.refreshNeeded = true;
    const dialogRef = this.dialog.open(InventoryLocationFormComponent, {
      width: '540px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.locationConsultsService.initializeForm();
    });
    this.locationConsultsService.selectedLocation = Object.assign({},location);;   
  }

  deleteLocation(location){
    this.refreshNeeded = true;
    if(confirm("¿Esta seguro de que quiere eliminar el registro?. Se eliminaran todos los registros con esta localizacion de la base de datos de inventario") == true){
      this.locationConsultsService.deleteLocation(location);
      this.deleteItemsLocation(location);
      this.toastrService.success('Operación ejecutada con exito','Eliminar localizacion');
    }
  }
  deleteItemsLocation(location){
    var content = this.itemsConsultsService.getItems(this.userId);
    var subscription = content.snapshotChanges().subscribe(inventoryLocation => {
      inventoryLocation.forEach( element => {
        var register = element.payload.toJSON();
        if(register["location"] == location){
          this.itemsConsultsService.deleteItem(element.key);
        }
      })
    });
  }
  datatableOptions(){
    this.dtOptions = {
      responsive: true,
      scrollY: (window.innerHeight - 20),
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      order: [[ 0, "desc" ], [ 1, "asc" ]],
      columnDefs: [
        { responsivePriority: 0, targets: 1 },
        { responsivePriority: 1, targets: 0 },
      ],
      initComplete: function(settings, json) {
        $('.loading').addClass('hidden');
        $('.table-responsive table').removeClass('hidden');
        var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
        $('div.dataTables_scrollBody').height( scrollY ); 
      }
    };
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  destroyTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.isRendered = false;
      this.getLocations()
    });
  }
}
