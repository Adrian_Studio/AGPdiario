import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { VehicleConsultsService } from '../../../services/dropdowns/vehicle-consults.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/users/user.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-vehicle-form',
  templateUrl: './vehicle-form.component.html',
  styleUrls: ['./vehicle-form.component.scss']
})
export class VehicleFormComponent implements OnInit {
  userId: string;
  keyBackUp: string;
  userIdBackup: string;
  constructor(public dialogRef: MatDialogRef<VehicleConsultsService>,
    public vehicleConsultsService: VehicleConsultsService,
    private toastrService: ToastrService,
    private userService: UserService,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
      } 
    });
  }


  onSubmit(vehicleForm: NgForm){
    if(this.vehicleConsultsService.selectedVehicle.$key == null){
      this.vehicleConsultsService.addVehicle(vehicleForm.value);
      this.toastrService.success('Operación ejecutada con exito','Añadir Vehículo')
      this.dialogRef.close();
    }else{
      this.vehicleConsultsService.updateVehicle(vehicleForm.value);
      this.toastrService.success('Operación ejecutada con exito','Editar Vehículo')
      this.dialogRef.close();
    }
  }

  resetForm(vehicleForm: NgForm){
    this.keyBackUp = vehicleForm.value.$key;
    this.userIdBackup = vehicleForm.value.userId;
    if (vehicleForm != null){
      vehicleForm.reset();
    }
    vehicleForm.value.$key = this.keyBackUp;
    vehicleForm.value.userId = this.userIdBackup;
  }
}

