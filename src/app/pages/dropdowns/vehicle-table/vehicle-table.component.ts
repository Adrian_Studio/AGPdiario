import { Component, OnInit, ViewChild } from '@angular/core';
import { VehicleConsultsService } from '../../../services/dropdowns/vehicle-consults.service';
import { AccountingActivityConsultsService } from '../../../services/accounting/accounting-activity-consults.service';
import { Vehicles } from '../../../models/dropdowns/vehicles';
import { ToastrService } from 'ngx-toastr';
import { VehicleFormComponent } from "../vehicle-form/vehicle-form.component";
import { MatDialog } from '@angular/material';
import { UserService } from '../../../services/users/user.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-vehicle-table',
  templateUrl: './vehicle-table.component.html',
  styleUrls: ['./vehicle-table.component.scss'],
  entryComponents:[ VehicleFormComponent ]
})
export class VehicleTableComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = { };
  vehiclesList: Vehicles[];
  userId: string;
  dtTrigger: Subject<any> = new Subject();
  isRendered: boolean = false;
  refreshNeeded: boolean = true;
  lengthDB: number;

  constructor(
      private vehicleConsultsService: VehicleConsultsService,
      private activityConsultsService: AccountingActivityConsultsService,
      private toastrService: ToastrService,
      private userService: UserService,
      public dialog: MatDialog,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.vehicleConsultsService.initializeForm();
        this.datatableOptions();
        this.getVehicles();
      } 
    }); 
  }
  
  getVehicles(){
    var content = this.vehicleConsultsService.getVehicles(this.userId);
    content.snapshotChanges().subscribe(vehicles => {
      if(this.refreshNeeded || vehicles.length != this.lengthDB){
        $('.loading').removeClass('hidden');
        $('.table-responsive table').addClass('hidden');
        if(this.isRendered){
          this.destroyTable();
        }else{
          this.vehiclesList = [];
          vehicles.forEach( element => {
            var register = element.payload.toJSON();
            register["$key"] = element.key;
            this.vehiclesList.push(register as Vehicles)
          })
          this.refreshNeeded = false;  
          this.dtTrigger.next();
          this.isRendered = true;     
          this.lengthDB = vehicles.length;
        }
      }
    });  
  }

  editVehicle(vehicle){
    this.refreshNeeded = true;
    const dialogRef = this.dialog.open(VehicleFormComponent, {
      width: '300px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.vehicleConsultsService.initializeForm();
    });
    this.vehicleConsultsService.selectedVehicle = Object.assign({},vehicle);;   
  }

  deleteVehicle(vehicle){
    this.refreshNeeded = true;
    if(confirm("¿Esta seguro de que quiere eliminar el registro?. Se eliminaran todos los registros con esta localizacion de la base de datos de inventario") == true){
      this.vehicleConsultsService.deleteVehicle(vehicle);
      this.deleteActivityVehicle(vehicle);
      this.toastrService.success('Operación ejecutada con exito','Eliminar localizacion');
    }
  }
  deleteActivityVehicle(vehicle){
    var content = this.activityConsultsService.getActivities(this.userId);
    var subscription = content.snapshotChanges().subscribe(vehicles => {
      vehicles.forEach( element => {
        var register = element.payload.toJSON();
        if(register["vehicle"] == vehicle){
          this.activityConsultsService.deleteActivity(element.key);
        }
      })
    });
  }
  datatableOptions(){
    this.dtOptions = {
      responsive: true,
      scrollY: (window.innerHeight - 20),
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      order: [[ 0, "desc" ], [ 1, "asc" ]],
      columnDefs: [
        { responsivePriority: 0, targets: 1 },
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: 2 },
      ],
      initComplete: function(settings, json) {
        $('.loading').addClass('hidden');
        $('.table-responsive table').removeClass('hidden');
        var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
        $('div.dataTables_scrollBody').height( scrollY ); 
      }
    };
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  destroyTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.isRendered = false;
      this.getVehicles()
    });
  }
}
