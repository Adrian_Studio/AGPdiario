import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { InventoryItemFormComponent } from "../inventory-item-form/inventory-item-form.component";
import { InventoryItemsConsultsService } from '../../../services/inventory/inventory-items-consults.service';
import { InventoryElementTypeConsultsService } from '../../../services/dropdowns/inventory-element-type-consults.service';
import { InventoryLocationConsultsService } from '../../../services/dropdowns/inventory-location-consults.service';
import { InventoryItemTableComponent } from '../inventory-item-table/inventory-item-table.component';
import { InventoryItem } from '../../../models/inventory/inventory-item';
import { InventoryElementType } from '../../../models/dropdowns/inventory-element-type';
import { InventoryLocation } from '../../../models/dropdowns/inventory-location';
import { UserService } from '../../../services/users/user.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-inventory-header',
  templateUrl: './inventory-header.component.html',
  styleUrls: ['./inventory-header.component.scss']
})
export class InventoryHeaderComponent implements OnInit {
  @ViewChild(InventoryItemTableComponent) itemTableChild: InventoryItemTableComponent;
  title:string = 'Inventario';
  itemListUncut: InventoryItem[];
  elementTypeListUncut: InventoryElementType[];
  locationListUncut: InventoryLocation[];
  userId: string;
  selectedPage: string;

  constructor(
    public dialog: MatDialog, 
    private itemsConsultsService: InventoryItemsConsultsService,
    private elementTypeConsultsService: InventoryElementTypeConsultsService,
    private locationConsultsService: InventoryLocationConsultsService,
    private userService: UserService,
    private route: ActivatedRoute, 
  ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.selectedPage = this.route.snapshot.data['selectedPage'];
        this.itemsConsultsService.initializeForm();
        this.getElementTypes();
        this.getLocations();
        this.getItems();
      } 
    }); 
  }
  openInputForm(type:string): void {
    const dialogRef = this.dialog.open(InventoryItemFormComponent, {
      width: '540px',
      panelClass: type,
      /*data: {
        type: type
      }*/
    });
    dialogRef.afterClosed().subscribe(result => {
      this.itemsConsultsService.initializeForm();
    });
  }
  getItems(){
    var content = this.itemsConsultsService.getItems(this.userId);
    content.snapshotChanges().subscribe(inventoryItem => {
      this.itemListUncut = [];
      inventoryItem.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        register["locationName"] = this.locationListUncut.find( location => location.$key === register['location'] ).location; 
        register["itemTypeName"] = this.elementTypeListUncut.find( type => type.$key === register['itemType'] ).elementType; 
        this.itemListUncut.push(register as InventoryItem)
      })
    });   
  }

  getElementTypes(){
    var content = this.elementTypeConsultsService.getElementTypes(this.userId);
    var subscription = content.snapshotChanges().subscribe(inventoryElementType => {
      this.elementTypeListUncut = [];
      inventoryElementType.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.elementTypeListUncut.push(register as InventoryElementType)
      });
    });
  }
  getLocations(){
    var content = this.locationConsultsService.getLocations(this.userId);
    var subscription = content.snapshotChanges().subscribe(inventoryLocation => {
      this.locationListUncut = [];
      inventoryLocation.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.locationListUncut.push(register as InventoryLocation)
      });
    });
  }
}
