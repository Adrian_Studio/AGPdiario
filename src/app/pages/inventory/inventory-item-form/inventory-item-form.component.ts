import { Component, OnInit, Inject, ChangeDetectorRef} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { InventoryItemsConsultsService } from '../../../services/inventory/inventory-items-consults.service';
import { InventoryElementTypeConsultsService } from '../../../services/dropdowns/inventory-element-type-consults.service';
import { InventoryLocationConsultsService } from '../../../services/dropdowns/inventory-location-consults.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { UserService } from '../../../services/users/user.service';
import { InventoryElementType } from 'src/app/models/dropdowns/inventory-element-type';
import { InventoryLocation } from 'src/app/models/dropdowns/inventory-location';
import { ARIA_DESCRIBER_PROVIDER_FACTORY } from '@angular/cdk/a11y';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import { MatCalendarCellCssClasses } from '@angular/material';



export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-inventory-item-form',
  templateUrl: './inventory-item-form.component.html',
  styleUrls: ['./inventory-item-form.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class InventoryItemFormComponent implements OnInit {
  userId: string;
  keyBackUp: string;
  userIdBackup: string;
  inventoryElementTypeList: InventoryElementType[];
  inventoryLocationList: InventoryLocation[];

  constructor(public dialogRef: MatDialogRef<InventoryItemFormComponent>,
    public itemsConsultsService: InventoryItemsConsultsService,
    public elementTypeConsultsService: InventoryElementTypeConsultsService,
    public locationConsultsService: InventoryLocationConsultsService,
    private toastrService: ToastrService,
    private userService: UserService,
    private dateAdapter: DateAdapter<Date>,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.getElementTypes();
        this.getLocations();
      } 
    });
    this.dateAdapter.getFirstDayOfWeek = () => { return 1; }
  }


  onSubmit(inventaryForm: NgForm){
    if (inventaryForm.value.acquisitionDate["_d"]){
      inventaryForm.value.acquisitionDate = inventaryForm.value.acquisitionDate["_d"].toISOString();
      
    }
    if (inventaryForm.value.lossDate){
      if (inventaryForm.value.lossDate["_d"]){
        inventaryForm.value.lossDate = inventaryForm.value.lossDate["_d"].toISOString();
      }
    }

    if(this.itemsConsultsService.selectedItem.$key == null){
      this.itemsConsultsService.addItem(inventaryForm.value);
      this.toastrService.success('Operación ejecutada con exito','Añadir Item')
      this.dialogRef.close();
    }else{
      this.itemsConsultsService.updateItem(inventaryForm.value);
      this.toastrService.success('Operación ejecutada con exito','Editar Item')
      this.dialogRef.close();
    }
  }

  resetForm(inventaryForm: NgForm){
    this.keyBackUp = inventaryForm.value.$key;
    this.userIdBackup = inventaryForm.value.userId;
    if (inventaryForm != null){
      inventaryForm.reset();
    }
    inventaryForm.value.$key = this.keyBackUp;
    inventaryForm.value.userId = this.userIdBackup;
  }

  getElementTypes(){
    var content = this.elementTypeConsultsService.getElementTypes(this.userId);
    content.snapshotChanges().subscribe(inventoryElementType => {
      this.inventoryElementTypeList = [];
      inventoryElementType.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.inventoryElementTypeList.push(register as InventoryElementType)
      });
    });
  }
  getLocations(){
    var content = this.locationConsultsService.getLocations(this.userId);
    content.snapshotChanges().subscribe(inventoryLocation => {
      this.inventoryLocationList = [];
      inventoryLocation.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.inventoryLocationList.push(register as InventoryLocation)
      });
    });
  }
  dateClass() {
    return (d: Date): MatCalendarCellCssClasses => {
      const day = d['_d'].getDay();
      if(day == 0 || day ==6){
        return 'weekend'
      }
    };
  }
}
