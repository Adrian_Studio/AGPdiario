import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges, SimpleChange, HostListener } from '@angular/core';
import { InventoryItemsConsultsService } from '../../../services/inventory/inventory-items-consults.service';
import { InventoryItem } from '../../../models/inventory/inventory-item';
import { InventoryElementType } from '../../../models/dropdowns/inventory-element-type';
import { InventoryLocation } from '../../../models/dropdowns/inventory-location';
import { ToastrService } from 'ngx-toastr';
import { InventoryItemFormComponent } from "../inventory-item-form/inventory-item-form.component";
import { MatDialog } from '@angular/material';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-inventory-item-table',
  templateUrl: './inventory-item-table.component.html',
  styleUrls: ['./inventory-item-table.component.scss'],
  entryComponents:[ InventoryItemFormComponent ],
})
export class InventoryItemTableComponent implements OnInit {
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @Input() itemListUncut: InventoryItem[];
  @Input() elementTypeListUncut: InventoryElementType[];
  @Input() locationListUncut: InventoryLocation[];
  @HostListener('window:resize', ['$event']) 
  onResize(event){
    var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
    $('div.dataTables_scrollBody').height( scrollY );   
  }
  itemListTable: InventoryItem[]; // Variable necesaría para el correcto renderizado de la tabla
  dtOptions: any = { };
  dtTrigger: Subject<any> = new Subject(); // Variable necesaría para reescribir la tabla
  cutTable: boolean = false;
  tableLenght: number;

  constructor(
      private itemsConsultsService: InventoryItemsConsultsService,
      private toastrService: ToastrService,
      public dialog: MatDialog,
    ) {

    }

  ngOnInit() {
    this.datatableOptions();
    if(this.itemListUncut){
      this.renderTable();
    } 
  }

  ngOnChanges(changes: SimpleChanges){
    $('.table-responsive table').addClass('hidden');
    $('.cutTable').addClass('hidden');
    $('.loading').removeClass('hidden');
    if (this.itemListUncut){
      if (this.dtElement.dtInstance){
        this.renderTable();
      } else{
        if(this.itemListUncut.length >= 100){
          this.itemListTable = this.itemListUncut.slice(0,100);
          this.cutTable = true;
          this.tableLenght = this.itemListUncut.length;
        }else{
          this.itemListTable = this.itemListUncut;
          this.cutTable = false; 
        }
        this.dtTrigger.next();
      }
    }
  }
  datatableOptions(){
    this.dtOptions = {
      responsive: true,
      scrollY: (window.innerHeight - 20),
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      order: [[ 0, "desc" ], [ 1, "asc" ], [ 2, "asc" ]],
      columnDefs: [
        { responsivePriority: 0, targets: 12 },
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 3, targets: 4 },
        { responsivePriority: 4, targets: 6 },
        { responsivePriority: 5, targets: 7 },
      ],
      initComplete: function(settings, json) {
        $('.loading').addClass('hidden');
        $('.table-responsive table').removeClass('hidden');
        $('.cutTable').removeClass('hidden');
        var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
        $('div.dataTables_scrollBody').height( scrollY );        
      }
    };
  }

  renderTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.cutTable = false;
      $('.table-responsive table').addClass('hidden');
      $('.cutTable').addClass('hidden');
      $('.loading').removeClass('hidden');
      if(this.itemListUncut.length >= 100){
        this.itemListTable = this.itemListUncut.slice(0,100);
        this.cutTable = true;
        this.tableLenght = this.itemListUncut.length;
      }else{
        this.itemListTable = this.itemListUncut;
        this.cutTable = false;
      }
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe(); 
  }

  copyItem(item){
    this.itemsConsultsService.initializeForm();
    const dialogRef = this.dialog.open(InventoryItemFormComponent, {
      width: '540px',
    });
    
    dialogRef.afterClosed().subscribe(result => {
      this.itemsConsultsService.initializeForm();
    });
    this.itemsConsultsService.selectedItem = Object.assign({},item)
    this.itemsConsultsService.selectedItem.$key = null;
    this.itemsConsultsService.selectedItem.acquisitionDate = null;
  }

  editItem(item){
    const dialogRef = this.dialog.open(InventoryItemFormComponent, {
      width: '540px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.itemsConsultsService.initializeForm();
    });
    this.itemsConsultsService.selectedItem = Object.assign({},item);
  }

  deleteItem(item){
    if(confirm("¿Esta seguro de que quiere eliminar el registro?") == true){
      this.itemsConsultsService.deleteItem(item);
      this.toastrService.success('Operación ejecutada con exito','Eliminar Item');
    }
  }

}
