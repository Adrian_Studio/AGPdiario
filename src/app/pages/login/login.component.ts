import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/users/user.service';
import { Router } from'@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email: any;
  password: any;
  registered: boolean;

  constructor(private userService: UserService, public router: Router) { }

  ngOnInit() {
  }

  onSubmitLogin(){
    this.userService.login(this.email, this.password)
      .then((result) => {
        this.checkIfRegistered(result['user']['uid']);
      }).catch((error) => {
        this.router.navigate(['/login']);
        alert('El usuario y la contraseña no coinciden');
      });
  }

  loginGoogle(){
    this.userService.loginGoogle().then(result => {
      this.checkIfRegistered(result.user.uid);   
    }).catch(error => {
      console.log(error);
    })
  }

  checkIfRegistered(uid: string){
    var content = this.userService.getUsers(uid);
    let registered: boolean;
    var subscription = content.snapshotChanges().forEach(concept => {
      registered = concept.length != 0;
      if(registered){
        this.router.navigate(['']);
        this.userService.isRegistered = true;
      }else{
        this.userService.logout();
        alert('Usuario no registrado');
      }
    });
  }
}
