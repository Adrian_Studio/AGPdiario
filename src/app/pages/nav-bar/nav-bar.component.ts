import { ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/users/user.service';
import { Router } from'@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit {

  title = 'AGPdiario';
  isAuthenticated: boolean = false; //It has to say false by default
  loadedSideNav: boolean = false;
  name: string;
  email: string;
  photo: string;
  mobileQuery: MediaQueryList;
  showAccounting: boolean = true;
  showInventory: boolean = true;
  showTrips: boolean = true;

  accountingNav = [{icon: 'fas fa-chart-bar', pageName: 'Graficos', pageLink: 'accountingGraphs'},
                   {icon: 'fas fa-table', pageName: 'Tabla', pageLink: 'accountingTable'},
                   {icon: 'fas fa-paperclip', pageName: 'Resumen Mensual', pageLink: 'accountingMonthlySummary'},
                   {icon: 'far fa-clipboard', pageName: 'Resumen Anual', pageLink: 'accountingAnnualSummary'}];

  vehicleNav = [{icon: 'fas fa-chart-line', pageName: 'Graficos', pageLink: 'vehicleGraphs'},
                {icon: 'fas fa-gas-pump', pageName: 'Combustible', pageLink: 'vehicleGas'},
                {icon: 'fas fa-tools', pageName: 'Mantenimiento', pageLink: 'vehicleMaintenance'},
                {icon: 'fas fa-key', pageName: 'Financiacion', pageLink: 'vehicleFinancing'}];

  accountingDropdownNav = [{icon: 'fas fa-cubes', pageName: 'Categorias', pageLink: 'accountingCategories'},
                           {icon: 'fas fa-cube', pageName: 'Conceptos', pageLink: 'accountingConcepts'},
                           {icon: 'fas fa-home', pageName: 'Viviendas', pageLink: 'houses'},
                           {icon: 'fas fa-car', pageName: 'Vehiculos', pageLink: 'vehicles'}];
                          
  inventoryDropdownNav = [{icon: 'fas fa-scroll', pageName: 'Tipos de elemento', pageLink: 'inventoryType'},
                          {icon: 'fas fa-map-marker-alt', pageName: 'Localizaciones', pageLink: 'inventoryLocation'}];
                          


                       

  private _mobileQueryListener: () => void;
  constructor(private changeDetectorRef: ChangeDetectorRef, private media: MediaMatcher, private toastr: ToastrService, public userService: UserService, private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit(){   
    let registered:boolean;
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.isAuthenticated = true;
        this.name = result.displayName;
        this.email = result.email;
        this.photo = result.photoURL;
      } else {
        this.isAuthenticated = false;
      }
      if(window.innerWidth > 600){
        setTimeout(() => { //Retraso para la ejecucion de una funcion
          this.loadedSideNav = true;
        }, 100);  //Tiempo en milisegundos
      }else{
        this.loadedSideNav = true;
      }
    });

  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  logout(){
    if(confirm("¿Esta seguro de que quiere desloguearse?") == true){
      this.userService.logout();
      this.router.navigate(['/login']);
    }
  }
}
