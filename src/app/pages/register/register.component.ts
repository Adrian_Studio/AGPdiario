import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/users/user.service';
import { Router } from'@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  email: any;
  password: any;

  constructor(private userService: UserService, public router: Router, private toastr: ToastrService) { }

  ngOnInit() {
  }

  onSubmitEmailRegister(){
    this.userService.register(this.email, this.password).then((result) => {
      this.checkIfRegistered(result['user']['uid'], result['user']['email']);
    })
  }
  registerGoogle(){
    this.userService.loginGoogle().then(result => {
      this.checkIfRegistered(result.user.uid, result.user.email);
    })
  }
  checkIfRegistered(uid: string, email: string){
    var content = this.userService.getUsers(uid);
    let registered: boolean;
    var subscription = content.snapshotChanges().forEach(concept => {
      registered = concept.length != 0;
      if(registered){
        this.router.navigate(['/']);
        //alert('Usuario ya registrado con anterioridad');
      }else{
        this.userService.addUser(email);
        this.toastr.success('Registro', 'Cuenta creada correctamente!',{ });
        this.router.navigate(['/']);
        this.userService.isRegistered = true;
      }
    });
  }
}
