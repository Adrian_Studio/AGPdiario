import { Component, OnInit, Inject, ChangeDetectorRef} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { TripsConsultsService } from '../../../services/trips/trips-consults.service';
import { CountriesConsultsService } from '../../../services/trips/countries-consults.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { UserService } from '../../../services/users/user.service';
import { Country } from '../../../models/trips/countries';
import { ARIA_DESCRIBER_PROVIDER_FACTORY } from '@angular/cdk/a11y';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import { MatCalendarCellCssClasses } from '@angular/material';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-trips-form',
  templateUrl: './trips-form.component.html',
  styleUrls: ['./trips-form.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class TripsFormComponent implements OnInit {
  userId: string;
  keyBackUp: string;
  userIdBackup: string;
  countriesList: Country[];

  constructor(public dialogRef: MatDialogRef<TripsFormComponent>,
    public tripsConsultsService: TripsConsultsService,
    public countriesConsultsService: CountriesConsultsService,
    private toastrService: ToastrService,
    private userService: UserService,
    private dateAdapter: DateAdapter<Date>,
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.getCountries();
      } 
    });
    this.dateAdapter.getFirstDayOfWeek = () => { return 1; }
  }


  onSubmit(tripForm: NgForm){
    if (tripForm.value.date && tripForm.value.date["_d"]){
      tripForm.value.date = tripForm.value.date["_d"].toISOString();
      
    }

    if(this.tripsConsultsService.selectedTrip.$key == null){
      this.tripsConsultsService.addTrip(tripForm.value);
      this.toastrService.success('Operación ejecutada con exito','Añadir Item')
      this.dialogRef.close();
    }else{
      this.tripsConsultsService.updateTrip(tripForm.value);
      this.toastrService.success('Operación ejecutada con exito','Editar Item')
      this.dialogRef.close();
    }
  }

  resetForm(inventaryForm: NgForm){
    this.keyBackUp = inventaryForm.value.$key;
    this.userIdBackup = inventaryForm.value.userId;
    if (inventaryForm != null){
      inventaryForm.reset();
    }
    inventaryForm.value.$key = this.keyBackUp;
    inventaryForm.value.userId = this.userIdBackup;
  }

  getCountries(){
    var content = this.countriesConsultsService.geCountries();
    content.snapshotChanges().subscribe(inventoryElementType => {
      let countriesListProcessing: any[] = [];
      inventoryElementType.forEach( element => {
        var register = element.payload.toJSON();
        countriesListProcessing.push(register as Country)
      });
      countriesListProcessing.sort((a, b) => { return a.country.localeCompare(b.country); })
      this.countriesList = countriesListProcessing;
    });
  }
  dateClass() {
    return (d: Date): MatCalendarCellCssClasses => {
      const day = d['_d'].getDay();
      if(day == 0 || day ==6){
        return 'weekend'
      }
    };
  }
}
