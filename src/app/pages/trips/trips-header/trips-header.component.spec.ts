import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripsHeaderComponent } from './trips-header.component';

describe('TripsHeaderComponent', () => {
  let component: TripsHeaderComponent;
  let fixture: ComponentFixture<TripsHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TripsHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
