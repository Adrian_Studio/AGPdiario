import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { TripsFormComponent } from "../trips-form/trips-form.component";
import { TripsConsultsService } from '../../../services/trips/trips-consults.service';
import { CountriesConsultsService } from '../../../services/trips/countries-consults.service';
import { TripsTableComponent } from '../trips-table/trips-table.component';
import { Trip } from '../../../models/trips/trips';
import { Country } from '../../../models/trips/countries';
import { UserService } from '../../../services/users/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-trips-header',
  templateUrl: './trips-header.component.html',
  styleUrls: ['./trips-header.component.scss']
})
export class TripsHeaderComponent implements OnInit {
  @ViewChild(TripsTableComponent) tripsTableChild: TripsTableComponent;
  title:string = 'Viajes';
  tripsListUncut: Trip[];
  countriesListUncut: any[];
  userId: string;
  selectedPage: string;

  constructor(
    public dialog: MatDialog, 
    private tripssConsultsService: TripsConsultsService,
    private countriesConsultsService: CountriesConsultsService,
    private userService: UserService,
    private route: ActivatedRoute, 
  ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.selectedPage = this.route.snapshot.data['selectedPage'];
        this.tripssConsultsService.initializeForm();
        this.getCountries();
        this.getTrips();
      } 
    }); 
  }
  openInputForm(type:string): void {
    const dialogRef = this.dialog.open(TripsFormComponent, {
      width: '540px',
      panelClass: type,
      /*data: {
        type: type
      }*/
    });
    dialogRef.afterClosed().subscribe(result => {
      this.tripssConsultsService.initializeForm();
    });
  }
  getTrips(){
    var content = this.tripssConsultsService.getTrips(this.userId);
    content.snapshotChanges().subscribe(trips => {
      this.tripsListUncut = [];
      trips.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        if(register['country']){
          register["countryName"] = this.countriesListUncut.find( country => country.code === register['country'] ).country; 
        }
        this.tripsListUncut.push(register as Trip)
      })
    });   
  }

  getCountries(){
    var content = this.countriesConsultsService.geCountries();
    var subscription = content.snapshotChanges().subscribe(countries => {
      let countriesListProcessing: any[] = [];
      countries.forEach( element => {
        var register = element.payload.toJSON();
        //register["$key"] = element.key;
        countriesListProcessing.push(register as Country)
      });
      countriesListProcessing.sort((a, b) => { return a.country.localeCompare(b.country); })
      this.countriesListUncut = countriesListProcessing;
    });
  }
}
