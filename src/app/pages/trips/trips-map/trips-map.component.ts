import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges, SimpleChange, HostListener } from '@angular/core';
import { Trip } from '../../../models/trips/trips';

declare var jQuery: any;

@Component({
  selector: 'app-trips-map',
  templateUrl: './trips-map.component.html',
  styleUrls: ['./trips-map.component.scss']
})



export class TripsMapComponent implements OnInit {
  @Input() tripsListUncut: Trip[];
  visitedCountries: any;
  markers: any = [];
  initialMapScale: any;
  mapScale: any;
  savedMap: any;
  mapMarkersScale: any[] = [1];
  constructor() { 
    
  }

  ngOnInit() {
    this.loadMap("world_mill", this.visitedCountries, this.markers);
  }

  ngOnChanges(changes: SimpleChanges){
    if (this.tripsListUncut){
        $('#world-map').empty()
        this.processData(this.tripsListUncut)
    }
  }
  loadMap(map, visitedCountries, markers){

    jQuery('#world-map').vectorMap({
        map: map,
        backgroundColor: '#589bd7',
        regionStyle: {
            initial: {
                fill            : '#e7dcd8',
                'fill-opacity'  : 1,
                stroke          : 'none',
                'stroke-width'  : 0,
                'stroke-opacity': 1
            }
        },
        series: {
          /*markers: [{
            attribute: 'fill',
            min: 1,
            max: 100
            },{
            attribute: 'r',
            scale: [1, 10],
            value: 2, 
            min: 1,
            max: 100
          }], */
          regions: [{
            values: visitedCountries,
            scale            : ['#00722e', '#00b347'],
            normalizeFunction: 'polynomial'
          }]
        },
        markerStyle: {
            initial: {
                fill: '#F8E23B',
                stroke: '#383f47',
                r: 2
            },
            hover: {
                fill: 'blue',
                "stroke-width": 2,
                cursor: 'pointer'
              },
        },
        markers: markers,
        /*onRegionClick: function (event, code, region) {
            console.log("El codigo del lugar al que intentas acceder es: "+region+" - "+code);
            var name ="";
            switch (code){
                case 'AF':
                    name = 'AFRICA';
                    break;
                case 'NA':
                    name = 'NORTH_AMERICA';
                    break;
                case 'OC':
                    name = 'OCEANIA';
                    break;
                case 'AS':
                    name = 'ASIA';
                    break;
                case 'EU':
                    name = 'EUROPE';
                    break;
                case 'SA':
                    name = 'SOUTH_AMERICA';
                    break;
                case 'GB':
                    name = 'UK_REGIONS';
                    break;
                default:
                    name = code;
            };
            $(".jvectormap-tip").remove();
            var obj = $('#world-map .jvectormap-container').data('mapObject');
            var previousMap = obj.series.regions[0].map.params.map;
            $('#world-map').empty();
            try {
                this.cargaMapa(name.toLowerCase()+"_mill", visitedCountries);
                this.savedMap = previousMap
            } catch (error) {
                console.log("El mapa que intentas cargar no existe");
                this.cargaMapa(previousMap, visitedCountries);
                jQuery('#world-map').vectorMap('get','mapObject').setFocus({region: code, animate: true});
            }
            //console.log(event);
            
            
            //$('#world-map').vectorMap('set', 'focus', 4.9, 0.2, 0.41);
           
            //rellenaTabla(code);

            
        },*/
        onRegionOver: function (e, code, region) {
            document.body.style.cursor = "pointer";
        },
        onRegionOut: function (e, code, region) {
            document.body.style.cursor = "default";
        },
        onViewportChange: function (e, scale) {
            //e.preventDefault();
        },
        onMarkerTipShow: function (e, tip, code) {
            //e.preventDefault();
        },
        onMarkerOver: function (e, code) {
            //e.preventDefault();
        },
        onMarkerOut: function (e, code) {
            e.preventDefault();
        },
        onMarkerClick: function (e, code) {
            e.preventDefault();
        },
        onMarkerSelected: function (e, code) {
            e.preventDefault();
        },  
        onLabelShow: function(e, label, code) {
            e.preventDefault();
        }
        
    });
    //$('#world-map .jvectormap-container').attr('r', 2);//.substring(6, 24);
  }

  processData(data){
    this.visitedCountries = {};
    let index: number = 0;
    this.markers = [];
    data.forEach(element => {
        if(!this.visitedCountries[element.country] && element.country){
            this.visitedCountries[element.country] = 1;
        }else if (element.country){
            this.visitedCountries[element.country] += 1
        }

        if(element.latitude && element.longitude){
            this.markers[index] = {};
            this.markers[index].latLng = [element.latitude, element.longitude];
            this.markers[index].name = element.place; 
            index +=1;
        }
    });
    this.loadMap("world_mill", this.visitedCountries, this.markers);
  }
}
