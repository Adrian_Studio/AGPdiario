import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges, SimpleChange, HostListener } from '@angular/core';
import { TripsConsultsService } from '../../../services/trips/trips-consults.service';
import { Trip } from '../../../models/trips/trips';
import { Country } from '../../../models/trips/countries';
import { ToastrService } from 'ngx-toastr';
import { TripsFormComponent } from "../trips-form/trips-form.component";
import { MatDialog } from '@angular/material';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-trips-table',
  templateUrl: './trips-table.component.html',
  styleUrls: ['./trips-table.component.scss'],
  entryComponents:[ TripsFormComponent ],
})
export class TripsTableComponent implements OnInit {
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @Input() tripsListUncut: Trip[];
  @Input() countriesListUncut: Country[];
  @HostListener('window:resize', ['$event']) 
  onResize(event){
    if (window.innerHeight - $('app-trips-table').offset().top - 20 < 200 ){
      $('div.dataTables_scrollBody').height( 200 );
    }else{
      var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
      $('div.dataTables_scrollBody').height( scrollY );
    } 
  }
  tripsListTable: Trip[]; // Variable necesaría para el correcto renderizado de la tabla
  dtOptions: any = { };
  dtTrigger: Subject<any> = new Subject(); // Variable necesaría para reescribir la tabla
  cutTable: boolean = false;
  tableLenght: number;

  constructor(
      private tripsConsultsService: TripsConsultsService,
      private toastrService: ToastrService,
      public dialog: MatDialog,
    ) {

    }

  ngOnInit() {
    this.datatableOptions();
    if(this.tripsListUncut){
      this.renderTable();
    } 
  }

  ngOnChanges(changes: SimpleChanges){
    $('.table-responsive table').addClass('hidden');
    $('.cutTable').addClass('hidden');
    $('.loading').removeClass('hidden');
    if (this.tripsListUncut){
      if (this.dtElement.dtInstance){
        this.renderTable();
      } else{
        /*if(this.tripsListUncut.length >= 100){
          this.tripsListTable = this.tripsListUncut.slice(0,100);
          this.cutTable = true;
          this.tableLenght = this.tripsListUncut.length;
        }else{*/
          this.tripsListTable = this.tripsListUncut;
          this.cutTable = false; 
        //}
        this.dtTrigger.next();
      }
    }
  }
  datatableOptions(){

    this.dtOptions = {
      responsive: true,
      scrollY: 1000,
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      order: [[ 0, "desc" ]],
      columnDefs: [
        { responsivePriority: 0, targets: 7 },
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: 3 },
        { responsivePriority: 3, targets: 2 },
        { responsivePriority: 4, targets: 1 },
      ],
      initComplete: function(settings, json) {
        $('.loading').addClass('hidden');
        $('.table-responsive table').removeClass('hidden');
        $('.cutTable').removeClass('hidden');   
        if (window.innerHeight - $('app-trips-table').offset().top - 20 < 200 ){
          $('div.dataTables_scrollBody').height( 200 );
        }else{
          var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
          $('div.dataTables_scrollBody').height( scrollY );
        }
      }
    };
  }

  renderTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.cutTable = false;
      $('.table-responsive table').addClass('hidden');
      $('.cutTable').addClass('hidden');
      $('.loading').removeClass('hidden');
      /*if(this.tripsListUncut.length >= 100){
        this.tripsListTable = this.tripsListUncut.slice(0,100);
        this.cutTable = true;
        this.tableLenght = this.tripsListUncut.length;
      }else{*/
        this.tripsListTable = this.tripsListUncut;
        this.cutTable = false;
      //}
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe(); 
  }

  copyTrip(trip){
    this.tripsConsultsService.initializeForm();
    const dialogRef = this.dialog.open(TripsFormComponent, {
      width: '540px',
    });
    
    dialogRef.afterClosed().subscribe(result => {
      this.tripsConsultsService.initializeForm();
    });
    this.tripsConsultsService.selectedTrip = Object.assign({},trip)
    this.tripsConsultsService.selectedTrip.$key = null;
  }

  editTrip(trip){
    const dialogRef = this.dialog.open(TripsFormComponent, {
      width: '540px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.tripsConsultsService.initializeForm();
    });
    this.tripsConsultsService.selectedTrip = Object.assign({},trip);
  }

  deleteTrip(trip){
    if(confirm("¿Esta seguro de que quiere eliminar el registro?") == true){
      this.tripsConsultsService.deleteTrip(trip);
      this.toastrService.success('Operación ejecutada con exito','Eliminar Item');
    }
  }

}
