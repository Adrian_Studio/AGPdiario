import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleFilterFormComponent } from './vehicle-filter-form.component';

describe('VehicleFilterFormComponent', () => {
  let component: VehicleFilterFormComponent;
  let fixture: ComponentFixture<VehicleFilterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleFilterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
