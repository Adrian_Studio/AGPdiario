import { Component, OnInit, Inject, ChangeDetectorRef} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContainer} from '@angular/material/dialog';
import { VehicleConsultsService } from '../../../services/dropdowns/vehicle-consults.service';
import { NgForm } from '@angular/forms';
import { UserService } from '../../../services/users/user.service';
import { VehicleFilterService } from '../../../services/accounting/vehicle-filter.service';
import { AccountingCategories } from 'src/app/models/dropdowns/accounting-categories';
import { AccountingConcepts } from 'src/app/models/dropdowns/accounting-concepts';
import { Houses } from 'src/app/models/dropdowns/houses';
import { Vehicles } from 'src/app/models/dropdowns/vehicles';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MatCalendarCellCssClasses } from '@angular/material';


export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-vehicle-filter-form',
  templateUrl: './vehicle-filter-form.component.html',
  styleUrls: ['./vehicle-filter-form.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class VehicleFilterFormComponent implements OnInit {
  userId: string;
  vehiclesList: Vehicles[];

  yearsList: number[];
  smallWindow = window.innerWidth < 400;
  selectedPage: string;

  constructor(public dialogRef: MatDialogRef<VehicleFilterFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public vehicleConsultsService: VehicleConsultsService,
    private userService: UserService,
    public filterService: VehicleFilterService,
    private dateAdapter: DateAdapter<Date>,
    ) { 
      this.yearsList = this.data["yearsList"];
      this.selectedPage = this.data["selectedPage"];
    }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.getVehicles();
        this.formManagement();
      } 
    });
    this.dateAdapter.getFirstDayOfWeek = () => { return 1; }
  }

  resetForm(filterForm: NgForm){
    if (filterForm != null){
      filterForm.reset();
    }
  }
  getSavedFilter(){
    this.filterService.selectSavedFilter();
  }

  onSubmit(filterForm: NgForm){
    
    this.filterService.selectedFilter = filterForm.value
    this.filterService.applyFilter();
    this.dialogRef.close();
  }

  getVehicles(){
    var content = this.vehicleConsultsService.getVehicles(this.userId);
    content.snapshotChanges().subscribe(vehicles => {
      this.vehiclesList = [];
      vehicles.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.vehiclesList.push(register as Vehicles)
      });
    });
  }
  formManagement(){
 
  }
  dateClass() {
    return (d: Date): MatCalendarCellCssClasses => {
      const day = d['_d'].getDay();
      if(day == 0 || day ==6){
        return 'weekend'
      }
    };
  }
}
