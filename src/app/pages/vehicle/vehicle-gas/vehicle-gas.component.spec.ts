import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleGasComponent } from './vehicle-gas.component';

describe('VehicleGasComponent', () => {
  let component: VehicleGasComponent;
  let fixture: ComponentFixture<VehicleGasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleGasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleGasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
