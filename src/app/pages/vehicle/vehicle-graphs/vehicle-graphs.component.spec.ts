import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleGraphsComponent } from './vehicle-graphs.component';

describe('VehicleGraphsComponent', () => {
  let component: VehicleGraphsComponent;
  let fixture: ComponentFixture<VehicleGraphsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleGraphsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleGraphsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
