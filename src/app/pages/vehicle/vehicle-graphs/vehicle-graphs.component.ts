import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label } from 'ng2-charts';
import { AccountingActivity } from '../../../models/accounting/accounting-activity';
import { AccountingCategories } from '../../../models/dropdowns/accounting-categories';
import { AccountingConcepts } from '../../../models/dropdowns/accounting-concepts';
import { Vehicles } from '../../../models/dropdowns/vehicles';
import { MaxLengthValidator } from '@angular/forms';

@Component({
  selector: 'app-vehicle-graphs',
  templateUrl: './vehicle-graphs.component.html',
  styleUrls: ['./vehicle-graphs.component.scss']
})
export class VehicleGraphsComponent implements OnInit {
  @Input() activityListUncut: AccountingActivity[];
  @Input() activityList: AccountingActivity[];
  @Input() vehiclesListUncut: Vehicles[];
  @Input() categoriesListUncut: AccountingCategories[];
  @Input() conceptsListUncut: AccountingConcepts[];
  totalKilometers: number;
  averageLitresPer100Km: number;
  percentLitresPer100Km: number;
  averageKmPerLitre: number;
  totalGasCost: number;
  totalMaintenanceCost: number;
  averagleCostPerLitre: number;
  totalGasLitres: number;
  totalFinanceCost: number;
  averageGasCostPerKm: number;
  percentGasCostPerKm: number;
  averageUseCostPerKm: number;
  averageCostPerKm: number;
  totalCost: number;
  
  constructor() { 
  }

  ngOnInit() {
  }
  
  ngOnChanges(changes: SimpleChanges){
    if(this.activityListUncut){
      this.processData(this.activityListUncut);
    }
  }

  processData(data:any){
    let litreCostMax: number = 0;
    let litreCostMin: number = 1000;
    let litresPer100KmMax: number = 0;
    let litresPer100KmMin: number = 1000;

    data.sort((a, b) => {return a.date.localeCompare(b.date)})
    this.totalKilometers = 0;
    this.totalGasCost = 0;
    this.totalGasLitres = 0;
    this.totalFinanceCost = 0;
    this.totalMaintenanceCost = 0;
    this.totalCost = 0;
    this.averageLitresPer100Km = 0;
    this.averageKmPerLitre = 0;
    this.averagleCostPerLitre = 0;
    this.averageGasCostPerKm = 0;
    this.averageUseCostPerKm = 0;
    this.averageCostPerKm = 0;
    var previousMilometer: number = 0;
    let initialGas:number = 0;

    data.forEach( element => {
      if(element.conceptKey == '-LeGQQpFpEMu8v6-xyEV'){ //Financiacion
        this.totalCost += element.value;
        this.totalFinanceCost += element.value;
      }else if(element.conceptKey == '-LeGQjCyoJAFgDnepuYo'){ // Combustible
        if(!element.milometer || element.milometer - previousMilometer > 3000){ // Consideramos que un coche de segunda mano minimo tendra 3000 km
          element.milometer = 0;
          initialGas += element.litres;
        };
        if(element.milometer > previousMilometer ){
          element.coveredMilometer = element.milometer - previousMilometer;
        }else{
          element.coveredMilometer = element.milometer
        }
        previousMilometer = element.milometer;
        this.totalCost += element.value;
        this.totalGasCost += element.value;
        this.totalGasLitres += element.litres;
        this.totalKilometers += element.coveredMilometer;
        if(litreCostMax < element.value/element.litres){
          litreCostMax = element.value/element.litres
        }
        if(litreCostMin > element.value/element.litres){
          litreCostMin = element.value/element.litres
        }
        if(litresPer100KmMax < 100 * element.litres/element.coveredMilometer && element.coveredMilometer){
          litresPer100KmMax = 100 * element.litres/element.coveredMilometer;
        }
        if(litresPer100KmMin > 100 * element.litres/element.coveredMilometer){
          litresPer100KmMin = 100 * element.litres/element.coveredMilometer;
        }
      }else{ //Mantenimiento
        this.totalCost += element.value;
        this.totalMaintenanceCost += element.value;
      } 
    });
    
    
    this.averageLitresPer100Km =  100 * (this.totalGasLitres - initialGas) / this.totalKilometers;
    this.averageKmPerLitre = this.totalKilometers / (this.totalGasLitres - initialGas);
    this.averagleCostPerLitre = this.totalGasCost / this.totalGasLitres;
    this.averageGasCostPerKm = this.totalGasCost / this.totalKilometers;
    this.averageUseCostPerKm = (this.totalGasCost + this.totalMaintenanceCost) / this.totalKilometers;
    this.averageCostPerKm = this.totalCost / this.totalKilometers;
    if(litreCostMax || litreCostMin || this.averagleCostPerLitre){
      this.percentGasCostPerKm = 100 * (this.averagleCostPerLitre - litreCostMin)/(litreCostMax - litreCostMin);
    }else{
      this.percentGasCostPerKm = 0;
    }
    if(litresPer100KmMax || litresPer100KmMin || this.averageLitresPer100Km){
      this.percentLitresPer100Km = 100 * (this.averageLitresPer100Km - litresPer100KmMin)/(litresPer100KmMax - litresPer100KmMin);
    }else{
      this.percentLitresPer100Km = 0;
    }
  }
}
