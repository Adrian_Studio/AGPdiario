import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AccountingActivityFormComponent } from "../../accounting/accounting-activity-form/accounting-activity-form.component";
import { VehicleFilterFormComponent } from "../../vehicle/vehicle-filter-form/vehicle-filter-form.component";
import { AccountingActivityConsultsService } from '../../../services/accounting/accounting-activity-consults.service';
import { AccountingCategoryConsultsService } from '../../../services/dropdowns/accounting-category-consults.service';
import { AccountingConceptConsultsService } from '../../../services/dropdowns/accounting-concept-consults.service';
import { VehicleConsultsService } from '../../../services/dropdowns/vehicle-consults.service';
import { VehicleFilterService } from '../../../services/accounting/vehicle-filter.service';
import { AccountingActivity } from '../../../models/accounting/accounting-activity';
import { AccountingCategories } from '../../../models/dropdowns/accounting-categories';
import { AccountingConcepts } from '../../../models/dropdowns/accounting-concepts';
import { Houses } from '../../../models/dropdowns/houses';
import { Vehicles } from '../../../models/dropdowns/vehicles';
import { UserService } from '../../../services/users/user.service';
import { ActivatedRoute } from '@angular/router';
import { VehicleFilter } from '../../../models/accounting/vehicle-filter';

@Component({
  selector: 'app-vehicle-header',
  templateUrl: './vehicle-header.component.html',
  styleUrls: ['./vehicle-header.component.scss'],
  entryComponents:[ 
    AccountingActivityFormComponent,
    VehicleFilterFormComponent,
   ]
})
export class VehicleHeaderComponent implements OnInit {
  title:string = 'Vehiculo';
  userId: string;
  activityListUncut: AccountingActivity[];
  activityList: AccountingActivity[];
  categoriesListUncut: AccountingCategories[];
  conceptsListUncut: AccountingConcepts[];
  housesListUncut: Houses[];
  vehiclesListUncut: Vehicles[];
  yearsList: number[] = [];
  selectedPage: string;

  constructor(
    public dialog: MatDialog, 
    private activityConsultsService: AccountingActivityConsultsService,
    public vehicleFilterService: VehicleFilterService,
    private categoryConsultsService: AccountingCategoryConsultsService,
    private conceptConsultsService: AccountingConceptConsultsService,
    private vehicleConsultsService: VehicleConsultsService,
    private userService: UserService,
    private route: ActivatedRoute,  
    ) { }

  ngOnInit() {
    this.userService.getInfoAccount().subscribe(result => {
      if (result){
        this.userId = result.uid;
        this.selectedPage = this.route.snapshot.data['selectedPage'];
        this.vehicleFilterService.emptyApliedForm();
        this.changeTitle(this.selectedPage);
        this.activityConsultsService.initializeForm();
        this.getCategories();
        this.getConcepts();
        this.getVehicles();
        this.getActivities();
      } 
    }); 
  }

  openInputForm(type:string): void {
    const dialogRef = this.dialog.open(AccountingActivityFormComponent, {
      width: '540px',
      panelClass: type,
      data: {
        type: type
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.activityConsultsService.initializeForm();
    });
  }
  
  openFiltersForm(): void {
    this.vehicleFilterService.selectAppliedFilter();
    const dialogRef = this.dialog.open(VehicleFilterFormComponent, {
      width: '540px',
      //panelClass: type,
      data: {
        yearsList: this.yearsList
      }
    });
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    })
    dialogRef.afterClosed().subscribe(result => {
      //this.accountingFilterService.initializeForm();
      if(this.vehicleFilterService.activeFilter){
        this.applyFilter();
      }
    });
  }


  getCategories(){
    var content = this.categoryConsultsService.getCategories(this.userId);
    var subscription = content.snapshotChanges().subscribe(category => {
      this.categoriesListUncut = [];
      category.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.categoriesListUncut.push(register as AccountingCategories)
      });
    });
  }

  getConcepts(){
    var content = this.conceptConsultsService.getConcepts(this.userId);
    var subscription = content.snapshotChanges().subscribe(concept => {
      this.conceptsListUncut = [];
      concept.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.conceptsListUncut.push(register as AccountingConcepts)
      });
    });
  }

  getVehicles(){
    var content = this.vehicleConsultsService.getVehicles(this.userId);
    var subscription = content.snapshotChanges().subscribe(concept => {
      this.vehiclesListUncut = [];
      concept.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        this.vehiclesListUncut.push(register as Vehicles)
      });
    });
  }

  getActivities(){
    var content = this.activityConsultsService.getActivities(this.userId);
    var subscription = content.snapshotChanges().subscribe(activity => {
      this.activityListUncut = [];
      this.activityList = [];
      activity.forEach( element => {
        var register = element.payload.toJSON();
        register["$key"] = element.key;
        register["category"] = this.categoriesListUncut.find( category => category.$key === register['categoryKey'] ).category;
        register["concept"] = this.conceptsListUncut.find( concept => concept.$key === register['conceptKey'] ).concept; 
        if(register["vehicle"]){
          this.activityListUncut.push(register as AccountingActivity)

          var date = new Date(register["date"]);
          var elementYear = date.getFullYear()
          if (this.yearsList.indexOf(elementYear) === -1) {this.yearsList.push(elementYear)};
        }
      })
      this.activityListUncut.sort((a, b) => {return b.date.localeCompare(a.date)})
      this.yearsList.sort();
      this.yearsList.reverse();
      this.applyFilter();
    });  
  }
  
  applyFilter(){
    let filter: VehicleFilter = this.vehicleFilterService.appliedFilter;
    if(this.vehicleFilterService.selectedToApplied && filter.vehicle){
      if (filter.vehicle ){
        this.activityList = this.activityListUncut.filter(element =>{ return element.vehicle == filter.vehicle});
      }
    }else{
      this.activityList = this.activityListUncut
    }
  }

  changeTitle(page: string){
    switch (page) {
      case 'vehicleGraphs':
        if (window.innerWidth <= 570){
          this.title = 'Vehíc. - Graficos'
        }else{
          this.title = 'Vehículo - Graficos'
        }
        break;
      case 'vehicleGas':
        if (window.innerWidth <= 570){
          this.title = 'Vehíc. - Combustible'
        }else{
          this.title = 'Vehículo - Combustible'
        }
        break;
      case 'vehicleMaintenance':
        if (window.innerWidth <= 570){
          this.title = 'Vehíc. - Mtto'
        }else{
          this.title = 'Vehículo - Mantenimiento'
        }
        break;
      case 'vehicleFinancing':
        if (window.innerWidth <= 570){
          this.title = 'Vehíc. - Financiación' 
        }else{
          this.title = 'Vehículo - Financiación' 
        }
        break;
       
      default:
        break;
    }
  }
}
