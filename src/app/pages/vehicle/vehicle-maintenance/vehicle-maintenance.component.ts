import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges, SimpleChange, HostListener } from '@angular/core';
import { AccountingActivityConsultsService } from '../../../services/accounting/accounting-activity-consults.service';
import { AccountingActivity } from '../../../models/accounting/accounting-activity';
import { AccountingCategories } from '../../../models/dropdowns/accounting-categories';
import { AccountingConcepts } from '../../../models/dropdowns/accounting-concepts';
import { ToastrService } from 'ngx-toastr';
import { AccountingActivityFormComponent } from "../../accounting/accounting-activity-form/accounting-activity-form.component";
import { MatDialog } from '@angular/material';
import { Subject, concat } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Vehicles } from '../../../models/dropdowns/vehicles';

@Component({
  selector: 'app-vehicle-maintenance',
  templateUrl: './vehicle-maintenance.component.html',
  styleUrls: ['./vehicle-maintenance.component.scss']
})
export class VehicleMaintenanceComponent implements OnInit {
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @Input() activityListUncut: AccountingActivity[];
  @Input() activityList: AccountingActivity[];
  @Input() vehiclesListUncut: Vehicles[];
  @Input() categoriesListUncut: AccountingCategories[];
  @Input() conceptsListUncut: AccountingConcepts[];
  @HostListener('window:resize', ['$event']) 
  onResize(event){
    var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
    $('div.dataTables_scrollBody').height( scrollY );
  }
  maintenanceListUncut: any;
  activityListTable: AccountingActivity[]; // Variable necesaría para el correcto renderizado de la tabla
  dtOptions: any = { };
  dtTrigger: Subject<any> = new Subject(); // Variable necesaría para reescribir la tabla
  cutTable: boolean = false;
  tableLenght: number;


  constructor(
      private activityConsultsService: AccountingActivityConsultsService,
      private toastrService: ToastrService,
      public dialog: MatDialog,
    ) { 

    }

  ngOnInit() {
    this.datatableOptions();
  }

  ngOnChanges(changes: SimpleChanges){
    $('.table-responsive table').addClass('hidden');
    $('.cutTable').addClass('hidden');
    $('.loading').removeClass('hidden');
    if (this.activityList){
      this.getMaintenanceData();
      //this.processMaintenanceData();
      if (this.dtElement.dtInstance){
        this.renderTable();
      } else{
        this.activityListTable = this.maintenanceListUncut;
        this.dtTrigger.next();
      }
    }
  }

 

  datatableOptions(){
    this.dtOptions = {
      responsive: true,
      scrollY: (window.innerHeight - 20),
      //scrollX: true,
      scrollCollapse: true,
      dom: '<"top"iflp>rt<"bottom"><"clear">',
      lengthChange: false,
      paging: false,
      order: [[ 0, "desc" ], [ 1, "asc" ], [ 2, "asc" ]],
      columnDefs: [
        {"targets":0, "type":"date-eu"},
        { responsivePriority: 0, targets: 0 },
        { responsivePriority: 1, targets: 6 },
        { responsivePriority: 2, targets: 3 },
        { responsivePriority: 3, targets: 1 },
        { responsivePriority: 4, targets: 2 },
        { responsivePriority: 5, targets: 5 },
      ],
      initComplete: function(settings, json) {
        $('.loading').addClass('hidden');
        $('.table-responsive table').removeClass('hidden');
        $('.cutTable').removeClass('hidden');
        var scrollY = (window.innerHeight - 20 - $('.dataTables_scrollBody').offset().top);
        $('div.dataTables_scrollBody').height( scrollY );        
      }
    };
  }
  
  renderTable(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      $('.table-responsive table').addClass('hidden');
      $('.cutTable').addClass('hidden');
      $('.loading').removeClass('hidden');
      this.activityListTable = this.maintenanceListUncut;
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  ngOnDestroy(){
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  copyActivity(activity: AccountingActivity){
    this.activityConsultsService.initializeForm();
    const dialogRef = this.dialog.open(AccountingActivityFormComponent, {
      width: '540px',
      panelClass: activity.type,
      data: {
        type: activity.type,
      }
    });
    
    dialogRef.afterClosed().subscribe(result => {
      this.activityConsultsService.initializeForm();
    });
    this.activityConsultsService.selectedActivity = Object.assign({},activity)
    this.activityConsultsService.selectedActivity.$key = null;
    this.activityConsultsService.selectedActivity.date = null;
    this.activityConsultsService.selectedActivity.category = this.activityConsultsService.selectedActivity.categoryKey;
    this.activityConsultsService.selectedActivity.concept = this.activityConsultsService.selectedActivity.conceptKey;
 
  }

  editActivity(activity: AccountingActivity){
    const dialogRef = this.dialog.open(AccountingActivityFormComponent, {
      width: '540px',
      panelClass: activity.type,
      data: {
        type: activity.type,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.activityConsultsService.initializeForm();
    });
    this.activityConsultsService.selectedActivity = Object.assign({},activity);
    this.activityConsultsService.selectedActivity.category = this.activityConsultsService.selectedActivity.categoryKey;
    this.activityConsultsService.selectedActivity.concept = this.activityConsultsService.selectedActivity.conceptKey;   
  }

  deleteActivity(activity: AccountingActivity){
    if(confirm("¿Esta seguro de que quiere eliminar el registro?") == true){
      this.activityConsultsService.deleteActivity(activity);
      this.toastrService.success('Operación ejecutada con exito','Eliminar movimiento');
    }
  }
  getMaintenanceData(){
    this.maintenanceListUncut = []
    this.activityList.forEach(element => {
      // Poner aqui la key referente al concepto que representa la tabla
      if(element.conceptKey != '-LeGQjCyoJAFgDnepuYo' && element.conceptKey != '-LeGQQpFpEMu8v6-xyEV'){
        var vehicle = this.vehiclesListUncut.find( vehicle => vehicle.$key === element.vehicle )
        element.vehicleName = vehicle.brand + " " + vehicle.model + " - "  + vehicle.plate ; 
        this.maintenanceListUncut.push(element as AccountingActivity)
      }
    });
    this.maintenanceListUncut.sort((a, b) => {return a.date.localeCompare(b.date)})
    this.maintenanceListUncut.sort((a, b) => {return a.vehicle.localeCompare(b.vehicle)})
  }
  processMaintenanceData(){
    var previousMilometer: number = 0;
    this.maintenanceListUncut.forEach(element => {
      if(element.milometer > previousMilometer ){
        element.coveredMilometer = element.milometer - previousMilometer;
      }else{
        element.coveredMilometer = element.milometer
      }
      previousMilometer = element.milometer;
    });
    this.maintenanceListUncut.reverse()
  }
}
