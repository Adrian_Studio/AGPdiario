import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';

import { UserService } from '../services/users/user.service';
import { AngularFireAuth } from 'angularfire2/auth'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private userService: UserService, private router: Router, fireAuth: AngularFireAuth){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      this.userService.getInfoAccount().subscribe(result => {
        if(result){
          var content = this.userService.getUsers(result.uid);
          let registered: boolean;
          var subscription = content.snapshotChanges().forEach(concept => {
            registered = concept.length != 0;
            if(registered){
              this.userService.isRegistered = true;
            }else{
              this.router.navigate(['/login']);
              return false;
            }
          });
        }else{
          this.router.navigate(['/login']);
          return false;    
        }
      });
    return true;
  }
  
}
