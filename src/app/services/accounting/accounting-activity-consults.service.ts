import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { AccountingActivity } from '../../models/accounting/accounting-activity';

@Injectable({
  providedIn: 'root'
})
export class AccountingActivityConsultsService {
  accountingActivityList: AngularFireList<any>;
  selectedActivity: AccountingActivity = new AccountingActivity();
  constructor(private angularFireDatabase: AngularFireDatabase) {   
  }

  initializeForm() {
    for(var index in this.selectedActivity){
      this.selectedActivity[index] = null;
    }
  }
  completeObject(activity: AccountingActivity){
    for(var index in activity){
      if(!activity[index]){activity[index]= null}
    }
  }
  //Get all the activities in the Database
  getActivities(userId: string){
    return this.accountingActivityList = this.angularFireDatabase.list(userId + '/accounting/activity');
  }
  //Add activity
  addActivity(activity: AccountingActivity){
    this.completeObject(activity)
    this.accountingActivityList.push({
      type: activity.type,
      categoryKey: activity.categoryKey,
      conceptKey: activity.conceptKey,
      value: activity.value,
      date: activity.date,
      house: activity.house,
      vehicle: activity.vehicle,
      milometer: activity.milometer,
      litres: activity.litres,
      remark: activity.remark,
    });
  }
  //Update activity
  updateActivity(activity: AccountingActivity){
    this.completeObject(activity)
    this.accountingActivityList.update(activity.$key,{
      type: activity.type,
      categoryKey: activity.categoryKey,
      conceptKey: activity.conceptKey,
      value: activity.value,
      date: activity.date,
      house: activity.house,
      vehicle: activity.vehicle,
      milometer: activity.milometer,
      litres: activity.litres,
      remark: activity.remark,
    });
  }
  //Delete activity
  deleteActivity(identifier: any){
    this.accountingActivityList.remove(identifier);
  }

}

