import { Injectable } from '@angular/core';
import { AccountingFilter } from '../../models/accounting/accounting-filter';

@Injectable({
  providedIn: 'root'
})
export class AccountingFilterService {
  selectedFilter: AccountingFilter = new AccountingFilter();  // Valores de filtro seleccionados
  appliedFilter: AccountingFilter = new AccountingFilter();   // Valores de filtro aplicados
  savedFilter: AccountingFilter = new AccountingFilter();     // Filtro guardado
  selectedToApplied: boolean = true;  // Necesidad de mover el filtro seleccionado a aplicado
  activeFilter: boolean = false; // Necesidad de aplicar filtro o no

  constructor() { 

  }

  initializeForm() {
    for(var index in this.selectedFilter){
      this.selectedFilter[index] = null;
    }
  }
  emptyApliedForm() {
    for(var index in this.appliedFilter){
      this.appliedFilter[index] = null;
    }
    this.activeFilter = false;
  }
  applyFilter(){
    // No puedo igualar la variable directamente porque al ser dos clases iguales 
    // les asocia la misma memoria y luego cambian al mismo tiempo
    this.selectedToApplied = true;
    this.activeFilter = false;
    if(this.selectedFilter.year && typeof this.selectedFilter.year != 'object'){
      this.selectedFilter.year =[this.selectedFilter.year]
    }
    for(var index in this.selectedFilter){
      this.appliedFilter[index] = this.selectedFilter[index];
      if(this.appliedFilter[index]){
        if((typeof this.appliedFilter[index] == "object" && this.appliedFilter[index].length != 0) || typeof this.appliedFilter[index] == "string"){
          this.activeFilter = true;
        }
      }
    }
    this.saveFilter();
  }
  selectAppliedFilter(){
    // No puedo igualar la variable directamente porque al ser dos clases iguales 
    // les asocia la misma memoria y luego cambian al mismo tiempo
    for(var index in this.selectedFilter){
      this.selectedFilter[index] = this.appliedFilter[index];
    }
    this.selectedToApplied = false;
  }
  saveFilter(){
    // No puedo igualar la variable directamente porque al ser dos clases iguales 
    // les asocia la misma memoria y luego cambian al mismo tiempo
    for(var index in this.appliedFilter){
      this.savedFilter[index] = this.appliedFilter[index];
    }
  }
  selectSavedFilter(){
    // No puedo igualar la variable directamente porque al ser dos clases iguales 
    // les asocia la misma memoria y luego cambian al mismo tiempo
    for(var index in this.savedFilter){
      this.selectedFilter[index] = this.savedFilter[index];
    }
  }

}
