import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { AccountingCategories } from '../../models/dropdowns/accounting-categories';

@Injectable({
  providedIn: 'root'
})
export class AccountingCategoryConsultsService {
  categories: AngularFireList<any>;
  selectedCategory: AccountingCategories = new AccountingCategories();
  constructor(private angularFireDatabase: AngularFireDatabase) {   
  }
  initializeForm() {
    for(var index in this.selectedCategory){
      this.selectedCategory[index] = null;
    }
  }
  //Get all the Category in the Database
  getCategories(userId){
    return this.categories = this.angularFireDatabase.list(userId + '/accounting/categories')
  }
  //Add Category
  addCategory(category: AccountingCategories){
    this.categories.push({
      type: category.type,
      category: category.category,
    });
  }
  //Update Category
  updateCategory(category: AccountingCategories){
    this.categories.update(category.$key,{
      type: category.type,
      category: category.category,
    });
  }
  //Delete Category
  deleteCategory(identifier: any){
    this.categories.remove(identifier);
  }
}

