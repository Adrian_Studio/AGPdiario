import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { AccountingConcepts } from '../../models/dropdowns/accounting-concepts';

@Injectable({
  providedIn: 'root'
})
export class AccountingConceptConsultsService {
  concepts: AngularFireList<any>;
  selectedConcept: AccountingConcepts = new AccountingConcepts();
  constructor(private angularFireDatabase: AngularFireDatabase) {   
  }
  initializeForm() {
    for(var index in this.selectedConcept){
      this.selectedConcept[index] = null;
    }
  }
  //Get all the Concept in the Database
  getConcepts(userId){
    return this.concepts = this.angularFireDatabase.list(userId + '/accounting/concepts')
  }
  //Add Concept
  addConcept(concept: AccountingConcepts){
    this.concepts.push({
      type: concept.type,
      category: concept.category,
      concept: concept.concept,
    });
  }
  //Update Concept
  updateConcept(concept: AccountingConcepts){
    this.concepts.update(concept.$key,{
      type: concept.type,
      category: concept.category,
      concept: concept.concept,
    });
  }
  //Delete Concept
  deleteConcept(identifier: any){
    this.concepts.remove(identifier);
  }
}
