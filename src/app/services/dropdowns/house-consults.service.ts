import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { Houses } from '../../models/dropdowns/houses';

@Injectable({
  providedIn: 'root'
})
export class HouseConsultsService {
  houses: AngularFireList<any>;
  selectedHouse: Houses = new Houses();
  constructor(private angularFireDatabase: AngularFireDatabase) {   
  }
  initializeForm() {
    for(var index in this.selectedHouse){
      this.selectedHouse[index] = null;
    }
  }
  //Get all the House in the Database
  getHouses(userId){
    return this.houses = this.angularFireDatabase.list(userId + '/houses')
  }
  //Add House
  addHouse(house: Houses){
    this.houses.push({
      street: house.street,
      number: house.number,
      floor: house.floor,
    });
  }
  //Update House
  updateHouse(house: Houses){
    this.houses.update(house.$key,{
      street: house.street,
      number: house.number,
      floor: house.floor,
    });
  }
  //Delete House
  deleteHouse(identifier: any){
    this.houses.remove(identifier);
  }
}
