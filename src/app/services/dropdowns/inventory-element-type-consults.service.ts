import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { InventoryElementType } from '../../models/dropdowns/inventory-element-type';

@Injectable({
  providedIn: 'root'
})
export class InventoryElementTypeConsultsService {
  inventoryElementTypeList: AngularFireList<any>;
  selectedElementType: InventoryElementType = new InventoryElementType();
  constructor(private angularFireDatabase: AngularFireDatabase) {   
  }
  initializeForm() {
    for(var index in this.selectedElementType){
      this.selectedElementType[index] = null;
    }
  }
  //Get all the ElementTypes in the Database
  getElementTypes(userId: string){
    return this.inventoryElementTypeList = this.angularFireDatabase.list(userId + '/inventory/ElementTypes')
  }
  //Add ElementType
  addElementType(elementType: InventoryElementType){
    this.inventoryElementTypeList.push({
      elementType: elementType.elementType,
    });
  }
  //Update ElementType
  updateElementType(elementType: InventoryElementType){
    this.inventoryElementTypeList.update(elementType.$key,{
      elementType: elementType.elementType,
    });
  }
  //Delete ElementType
  deleteElementType(identifier: any){
    this.inventoryElementTypeList.remove(identifier);
  }
}
