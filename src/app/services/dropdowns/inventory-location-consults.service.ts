import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { InventoryLocation } from '../../models/dropdowns/inventory-location';

@Injectable({
  providedIn: 'root'
})
export class InventoryLocationConsultsService {
  inventoryLocation: AngularFireList<any>;
  selectedLocation: InventoryLocation = new InventoryLocation();
  constructor(private angularFireDatabase: AngularFireDatabase) {   
  }
  initializeForm() {
    for(var index in this.selectedLocation){
      this.selectedLocation[index] = null;
    }
  }
  //Get all the Locations in the Database
  getLocations(userId){
    return this.inventoryLocation = this.angularFireDatabase.list(userId + '/inventory/Locations')
  }
  //Add Location
  addLocation(location: InventoryLocation){
    this.inventoryLocation.push({
      location: location.location,
    });
  }
  //Update Location
  updateLocation(location: InventoryLocation){
    this.inventoryLocation.update(location.$key,{
      location: location.location,
    });
  }
  //Delete Location
  deleteLocation(identifier: any){
    this.inventoryLocation.remove(identifier);
  }
}
