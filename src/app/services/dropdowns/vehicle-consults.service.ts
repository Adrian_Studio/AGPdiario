import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { Vehicles } from '../../models/dropdowns/vehicles';

@Injectable({
  providedIn: 'root'
})
export class VehicleConsultsService {
  vehicles: AngularFireList<any>;
  selectedVehicle: Vehicles = new Vehicles();
  constructor(private angularFireDatabase: AngularFireDatabase) {   
  }
  initializeForm() {
    for(var index in this.selectedVehicle){
      this.selectedVehicle[index] = null;
    }
  }
  //Get all the Vehicle in the Database
  getVehicles(userId){
    return this.vehicles = this.angularFireDatabase.list(userId + '/vehicles')
  }
  //Add Vehicle
  addVehicle(vehicle: Vehicles){
    this.vehicles.push({
      brand: vehicle.brand,
      model: vehicle.model,
      plate: vehicle.plate,
    });
  }
  //Update Vehicle
  updateVehicle(vehicle: Vehicles){
    this.vehicles.update(vehicle.$key,{
      brand: vehicle.brand,
      model: vehicle.model,
      plate: vehicle.plate,
    });
  }
  //Delete Vehicle
  deleteVehicle(identifier: any){
    this.vehicles.remove(identifier);
  }
}
