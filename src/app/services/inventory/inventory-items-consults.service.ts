import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { InventoryItem } from '../../models/inventory/inventory-item';

@Injectable({
  providedIn: 'root'
})
export class InventoryItemsConsultsService {
  inventoryItemList: AngularFireList<any>;
  selectedItem: InventoryItem = new InventoryItem();
  constructor(private angularFireDatabase: AngularFireDatabase) {   
  }

  initializeForm() {
    for(var index in this.selectedItem){
      this.selectedItem[index] = null;
    }
  }
  completeObject(item: InventoryItem){
    for(var index in item){
      if(!item[index]){item[index]= null}
    }
  }
  //Get all the items in the Database
  getItems(userId: string){
    return this.inventoryItemList = this.angularFireDatabase.list(userId + '/inventory/Items');
  }
  //Add item
  addItem(item: InventoryItem){
    this.completeObject(item)
    this.inventoryItemList.push({
      location: item.location,
      itemType: item.itemType,
      item: item.item,
      acquisitionPlace: item.acquisitionPlace,
      model: item.model,
      identifier: item.identifier,
      acquisitionValue: item.acquisitionValue,
      acquisitionDate: item.acquisitionDate,
      lossValue: item.lossValue,
      lossDate: item.lossDate,
      state: item.state,
      remark: item.remark,
    });
  }
  //Update item
  updateItem(item: InventoryItem){
    this.completeObject(item)
    this.inventoryItemList.update(item.$key,{
      location: item.location,
      itemType: item.itemType,
      item: item.item,
      acquisitionPlace: item.acquisitionPlace,
      model: item.model,
      identifier: item.identifier,
      acquisitionValue: item.acquisitionValue,
      acquisitionDate: item.acquisitionDate,
      lossValue: item.lossValue,
      lossDate: item.lossDate,
      state: item.state,
      remark: item.remark,
    });
  }
  //Delete item
  deleteItem(identifier: any){
    this.inventoryItemList.remove(identifier);
  }

}
