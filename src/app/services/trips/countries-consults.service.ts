import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { Country } from '../../models/trips/countries';

@Injectable({
  providedIn: 'root'
})
export class CountriesConsultsService {
  countriesList: AngularFireList<any>;
  constructor(private angularFireDatabase: AngularFireDatabase) {   
  }

  //Get all the counties in the Database
  geCountries(){
    return this.countriesList = this.angularFireDatabase.list('common/countries');
  }
}
