import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { Trip } from '../../models/trips/trips';

@Injectable({
  providedIn: 'root'
})
export class TripsConsultsService {
  tripsList: AngularFireList<any>;
  selectedTrip: Trip = new Trip();
  constructor(private angularFireDatabase: AngularFireDatabase) {   
  }

  initializeForm() {
    for(var index in this.selectedTrip){
      this.selectedTrip[index] = null;
    }
  }
  completeObject(trip: Trip){
    for(var index in trip){
      if(!trip[index]){trip[index]= null}
    }
  }
  //Get all the trips in the Database
  getTrips(userId: string){
    return this.tripsList = this.angularFireDatabase.list(userId + '/trips');
  }
  //Add trip
  addTrip(trip: Trip){
    this.completeObject(trip)
    this.tripsList.push({
      date: trip.date,
      continent: trip.continent,
      country: trip.country,
      place: trip.place,
      longitude: trip.longitude,
      latitude: trip.latitude,
      remark: trip.remark,
    });
  }
  //Update trip
  updateTrip(trip: Trip){
    this.completeObject(trip)
    this.tripsList.update(trip.$key,{
      date: trip.date,
      continent: trip.continent,
      country: trip.country,
      place: trip.place,
      longitude: trip.longitude,
      latitude: trip.latitude,
      remark: trip.remark,
    });
  }
  //Delete trip
  deleteTrip(identifier: any){
    this.tripsList.remove(identifier);
  }

}
