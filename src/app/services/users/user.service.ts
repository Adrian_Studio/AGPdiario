import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth'
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/map';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';




@Injectable({
  providedIn: 'root'
})
export class UserService {
  usersList: AngularFireList<any>;
  isRegistered: boolean;
  constructor(
    public fireAuth: AngularFireAuth,
    private angularFireDatabase: AngularFireDatabase,
    ) { }

  logout(){
    this.isRegistered = false;
    return this.fireAuth.auth.signOut();
  }

  register(email: string, password: string){
    return new Promise((resolve, reject) =>{
      this.fireAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(result => {
        resolve(result);
      }, error =>{
        reject(error);
      });
    });
  }

  getInfoAccount(){
    return this.fireAuth.authState.map(auth => auth);
  }
  login(email: string, password: string){
    return new Promise((resolve, reject) =>{
      this.fireAuth.auth.signInWithEmailAndPassword(email, password)
      .then(result => {
        resolve(result);
      }, error =>{
        reject(error);
      });
    });
  }
  loginGoogle(){
    return this.fireAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  //Get all the users in the Database
  getUsers(uid){
    return this.usersList = this.angularFireDatabase.list('users/' + uid);
  }
  //Add user
  addUser(email: string){
    this.usersList.set('email', email);
  }
  //Delete user
  deleteUser(uid: any){
    this.usersList.remove(uid);
    this.angularFireDatabase.list(uid);
  }
}
