/*jVectorMap*/
document.write("<script type='text/javascript' src='./jquery-jvectormap-2.0.3.min.js'></script"+">");

/*Mapas del mundo*/
document.write("<script type='text/javascript' src='./maps/jVectorMap-World-Continents.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-World-Countries.js'></script"+">");

/*Mapas de continente*/
document.write("<script type='text/javascript' src='./maps/jVectorMap-Africa-Countries.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Asia-Countries.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Europe-Countries.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-NorthAmerica-Countries.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Oceania-Countries.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-SouthAmerica-Countries.js'></script"+">");

/*Mapas de paises*/
document.write("<script type='text/javascript' src='./maps/jVectorMap-Australia-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Austria-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Belgium-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Canada-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-China-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Colombia-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Denmark-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-France-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Germany-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-India-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Italy-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Netherlands-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-NewZealand-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Norway-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Poland-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Portugal-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Russia-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-SouthAfrica-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-SouthKorea-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Spain-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Sweden-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Switzerland-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Thailand-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-UnitedKingdom-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-UnitedStatesOfAmerica-Provinces.js'></script"+">");
document.write("<script type='text/javascript' src='./maps/jVectorMap-Venezuela-Provinces.js'></script"+">");

